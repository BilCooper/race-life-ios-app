//
//  EventDetailView.m
//  eventmanagement
//
//  Created by Nick Schulze on 2/22/15.
//  Copyright (c) 2015 infusion infotech. All rights reserved.
//

#import "EventDetailView.h"

@implementation EventDetailView
@synthesize userLocation;
@synthesize parse;
@synthesize delegate;

-(id)init:(PFObject*)_object withShift:(int)_shift andLine:(BOOL)lineSwitch
{
    self = [super init];
    if (self)
    {
        object = _object;
        
        UIScreen* main = [UIScreen mainScreen];
        width = main.bounds.size.width;
        self.frame = CGRectMake(0, _shift, width, 84);
        
        int modifier = 0;
        if (lineSwitch)
        {
            self.frame = CGRectMake(10, _shift, width-10, 84);
            modifier = 10;
        }
        self.backgroundColor = [UIColor whiteColor];
        
        userName = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, width-20-modifier, 24)];
        userName.text = object[@"UserName"];
        [userName setFont:[UIFont fontWithName:@"Avenir-Heavy" size:14.0]];
        [userName setTextColor:[UIColor lightBlue]];
        [self addSubview:userName];
        
        UIFont *font = [UIFont fontWithName:@"Avenir-Heavy" size:14.0];
        NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                         NSForegroundColorAttributeName: [UIColor lightBlue]};
        NSString *text = userName.text;
        CGSize textSize = [text sizeWithAttributes: userAttributes];
        
        UIView* userbounding = [[UIView alloc] initWithFrame:CGRectMake(3, 5, textSize.width+10, textSize.height+4)];
        userbounding.backgroundColor = [UIColor clearColor];
        userbounding.layer.cornerRadius = (textSize.height+4)/2;
        userbounding.layer.borderColor = [UIColor lightBlue].CGColor;
        userbounding.layer.borderWidth = 1;
        [self addSubview:userbounding];
        
        NSDate *updated = [object createdAt];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM d, h:mm a"];
        
        time = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width-80, 0, 70, 30)];
        time.text = [NSString stringWithFormat:@"%@", [dateFormat stringFromDate:updated]];
        [time setTextAlignment:NSTextAlignmentRight];
        time.textColor = [UIColor black];
        time.alpha = .25;
        time.backgroundColor = [UIColor clearColor];
        [time setFont:[UIFont fontWithName:@"Avenir" size:8.0]];
        [self addSubview:time];
        
        comment = [[UITextView alloc] initWithFrame:CGRectMake(2, 21, width-10-modifier, 63)];
        [comment setUserInteractionEnabled:NO];
        comment.text = object[@"Comment"];
        [comment setFont:[UIFont fontWithName:@"Avenir-Light" size:13.0]];
        comment.backgroundColor = [UIColor clearColor];
        [self addSubview:comment];
        
        if ([object[@"hasImage"] boolValue])
        {
            image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 80, width-20-modifier, width-20-modifier)];
            image.contentMode = UIViewContentModeScaleAspectFill;
            image.layer.masksToBounds = YES;
            PFFile* file = object[@"Image"];
            [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                if (!error) {
                    UIImage *img = [UIImage imageWithData:data];
                    [image setImage:img];
                }
            }];
            [self addSubview:image];
            self.frame = CGRectMake(modifier, _shift, width-modifier, width+69-modifier);
            shift = width+70-modifier;
            
            button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width, width+70-modifier)];
            
            imagePopout = [[UIButton alloc] initWithFrame:CGRectMake(10, 80, width-20-modifier, width-20-modifier)];
            [imagePopout addTarget:self action:@selector(imageTap) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:imagePopout];
            
            UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, width+69, width-modifier, 1)];
            line.backgroundColor = [UIColor gray];
            [self addSubview:line];
        }
        else
        {
            shift = 84;
            
            button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width, 84)];
            
            UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 83, width-modifier, 1)];
            line.backgroundColor = [UIColor gray];
            [self addSubview:line];
        }
        
        button.backgroundColor = [UIColor clearColor];
        [button addTarget:self action:@selector(comments) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
        UIButton* button2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width-20-modifier, 44)];
        button2.backgroundColor = [UIColor clearColor];
        [button2 addTarget:self action:@selector(userProfile) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button2];
    }
    return self;
}

-(void)imageTap
{
    //[[self delegate] imageTap: image.image];
}

-(void)comments
{
    [[self delegate] goToEventComments:object];
}

-(int)getShift
{
    return shift;
}

-(void)userProfile
{
    [parse getUser:object[@"UserId"]];
}

@end