//
//  EventFeedViewController.h
//  Do-Invision
//
//  Created by Rupesh on 7/2/17.
//  Copyright © 2017 Mark Feaver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNGridMenu.h"
#import <QuickLook/QuickLook.h>
@interface EventFeedViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,RNGridMenuDelegate,QLPreviewControllerDelegate,QLPreviewControllerDataSource>
@property(nonatomic,strong)NSDictionary *event;
@end
