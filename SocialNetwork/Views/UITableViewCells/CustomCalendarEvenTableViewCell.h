//
//  CustomCalendarEvenTableViewCell.h
//  SocialNetwork
//
//  Created by Yasir on 12/22/17.
//  Copyright © 2017 Sugarsage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCalendarEvenTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgCell;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblMiles;




@end
