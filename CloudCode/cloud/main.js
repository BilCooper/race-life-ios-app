//version 2.0

require('./installation.js');
require('./activity.js');
require('./photo.js');
require('./masterKeyFunctions.js');
require('./pushNotification.js');


Parse.Cloud.afterSave("Activity", function(request) {
  //var messageText = request.object.get('message');
  var usersReceived = request.object.get('toUser');
  console.log("User received: " + usersReceived.id);

  var currentUser = request.user;
  
  console.log("User: " + currentUser.id);

  var toUser = request.object.get('toUser');
  var fromUser =  request.object.get('fromUser'); 

    if (toUser.id != fromUser.id) {               
        if (request.object.get("type") === "comment") {
            console.log("1 comment");
            var payload = {
                                alert: request.user.get('displayName') + ' commented on your photo',
                                //alert: request.user.get('displayName') + ' has also commented on ' + toUser.get('displayName') + "'s photo", // Set our alert message.
                                badge: 'Increment', // Increment the target device's badge count.
                                p: 'a', // Payload Type: Activity
                                t: 'c', // Activity Type: Comment
                                sound:'default',
                                fu: request.object.get('fromUser').id, // From User
                                pid: request.object.get('photo').id // Photo Id
            };
        } else 
        if (request.object.get("type") === "comment-video") {
            console.log("2 comment video");
            var payload = {
                                alert: request.user.get('displayName') + ' commented on your video',
                                //alert: request.user.get('displayName') + ' has also commented on ' + toUser.get('displayName') + "'s video", // Set our alert message.
                                badge: 'Increment', // Increment the target device's badge count.
                                // The following keys help Netzwierk load the correct photo in response to this push notification.
                                p: 'a', // Payload Type: Activity
                                t: 'c', // Activity Type: Comment
                                sound:'default',
                                fu: request.object.get('fromUser').id, // From User
                                pid: request.object.get('photo').id // Photo Id
            };
        } else 
        if (request.object.get("type") === "comment-post") {
            console.log("3 comment post");
            var payload = {
                                //alert: request.user.get('displayName') + ' has also commented on ' + toUser.get('displayName') + "'s post", // Set our alert message.
                                alert: request.user.get('displayName') + ' commented on your post',
                                badge: 'Increment', // Increment the target device's badge count.
                                // The following keys help Netzwierk load the correct photo in response to this push notification.
                                p: 'a', // Payload Type: Activity
                                t: 'c', // Activity Type: Comment
                                sound:'default',
                                fu: request.object.get('fromUser').id, // From User
                                pid: request.object.get('photo').id // Photo Id
            };
        } else 
        if (request.object.get("type") === "like-post") {
            console.log("7");
            var payload = {
                                alert: request.user.get('displayName') + ' liked your post',
                                badge: 'Increment', // Increment the target device's badge count.
                                // The following keys help Netzwierk load the correct photo in response to this push notification.
                                p: 'a', // Payload Type: Activity
                                t: 'l', // Activity Type: Comment
                                sound:'default',
                                fu: request.object.get('fromUser').id, // From User
                                pid: request.object.get('photo').id // Photo Id
            };
        } else 
        if (request.object.get("type") === "follow") {
            console.log("9");
            var payload = {
                                alert: request.user.get('displayName') + " followed you",
                                badge: 'Increment', // Increment the target device's badge count.
                                // The following keys help Netzwierk load the correct photo in response to this push notification.
                                p: 'a', // Payload Type: Activity
                                t: 'f', // Activity Type: Comment
                                sound:'default',
                                fu: request.object.get('fromUser').id, // From User
                                //pid: request.object.get('photo').id // Photo Id
            };
        }
                        
    }
    else {
        if (request.object.get("type") === "comment") {
            console.log("4 comment");
            var payload = {
                                alert: request.user.get('displayName') + ' commented on a photo you also commented on.', // Set our alert message.
                                badge: 'Increment', // Increment the target device's badge count.
                                // The following keys help Netzwierk load the correct photo in response to this push notification.
                                p: 'a', // Payload Type: Activity
                                t: 'c', // Activity Type: Comment
                                sound:'default',
                                fu: request.object.get('fromUser').id, // From User
                                pid: request.object.get('photo').id // Photo Id
            };
        }
        else if (request.object.get("type") === "comment-video") {
            console.log("5 comment video");
            var payload = {
                                alert: request.user.get('displayName') + ' commented on a video you also commented on.', // Set our alert message.
                                badge: 'Increment', // Increment the target device's badge count.
                                // The following keys help Netzwierk load the correct photo in response to this push notification.
                                p: 'a', // Payload Type: Activity
                                t: 'c', // Activity Type: Comment
                                sound:'default',
                                fu: request.object.get('fromUser').id, // From User
                                pid: request.object.get('photo').id // Photo Id
            };
        }
        else if (request.object.get("type") === "comment-post") {
            console.log("6 comment post");
            var payload = {
                                alert: request.user.get('displayName') + ' commented on a post you also commented on.', // Set our alert message.
                                badge: 'Increment', // Increment the target device's badge count.
                                // The following keys help Netzwierk load the correct photo in response to this push notification.
                                p: 'a', // Payload Type: Activity
                                t: 'c', // Activity Type: Comment
                                sound:'default',
                                fu: request.object.get('fromUser').id, // From User
                                pid: request.object.get('photo').id // Photo Id
            };
        } 
                        else if (request.object.get("type") === "like-post") {
                            console.log("8");
                            var payload = {
                                alert: request.user.get('displayName') + " liked on a post you also liked on",
                                badge: 'Increment', // Increment the target device's badge count.
                                // The following keys help Netzwierk load the correct photo in response to this push notification.
                                p: 'a', // Payload Type: Activity
                                t: 'c', // Activity Type: Comment
                                sound:'default',
                                fu: request.object.get('fromUser').id, // From User
                                pid: request.object.get('photo').id // Photo Id
                            };
                         } 
    }

//var userQuery = Parse.Object.extend('User');
// var pushQuery = new Parse.Query(Parse.User);
// pushQuery.equalTo('objectId', "DZP8Kkrj4j");

var pushQuery = new Parse.Query(Parse.Installation);
pushQuery.equalTo('user', usersReceived);
//pushQuery.notEqualTo('user', currentUser);
pushQuery.find({useMasterKey: true,
    success: function(results){
        if (results.length > 0) {
          console.log("Length: " + results.length);
        } else {
            console.log("0000");
        }
    }
});





//   var pushQuery = new Parse.Query(Parse.Installation);
//   //pushQuery.equalTo('user', usersReceived);
//   pushQuery.notEqualTo('user', currentUser);
  

  console.log("push: " + pushQuery);

  Parse.Push.send({
    where: pushQuery, // Set our Installation query
    data: payload
    // data: {
    //   alert: "New message: " + messageText
    // }
  }, { useMasterKey: true}).then(() => {
      // Push was successful
      console.log('weeee!');
  }, (e) => {
      console.log("Error!!!: " + e);
  });
});