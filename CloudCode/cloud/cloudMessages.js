Parse.Cloud.beforeSave('Activity', function(request, response) {
  
  var currentUser = request.user;
  var objectUser = request.object.get('fromUser');
  
  console.log("User: " + currentUser);
  console.log("From: " + objectUser);


  if(!currentUser || !objectUser) {
    response.error('An Activity should have a valid fromUser.');
  } else if (currentUser.id === objectUser.id) {
  
      if (request.object.get("type") === "comment" || request.object.get("type") === "comment-video"|| request.object.get("type") === "comment-post") {
        var photo = request.object.get('photo');
        var dummy = request.object.get('photoId');
        var photoId = photo.id;
        if (dummy) {
      //object has  id string
        }
        else {
        request.object.set('photoId', photoId);
      }
      }
      
      response.success();
        
  }
   else {
    response.error('Cannot set fromUser on Activity to a user other than the current user.');
  }
  
});
  
Parse.Cloud.afterSave('Activity', function(request) {
  // Only send push notifications for new activities
  if (request.object.existed()) {
    return;
  }
  
  var toUser = request.object.get('toUser');
  var fromUser =  request.object.get('fromUser');   

  console.log("To User: " + toUser.id);
  console.log("From User: " + fromUser.id); 

var thirdQuery = new Parse.Query(Parse.Installation);
console.log("THIRD: " + newUser.id);
thirdQuery.equalTo('user', toUser);

  Parse.Push.send({
    where: toUser,
    data: {
      alert: "Test",
      badge: "Increment"
    }
  }, { useMasterKey: true })
  .then(function() {
      response.success("Push was sent successfully.")
  }, function(error) {
      response.error("Push failed to send with error: " + error.message);
  });

});
