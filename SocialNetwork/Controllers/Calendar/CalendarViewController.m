//
//  CalendarViewController.m
//  Do-Invision
//
//  Created by Rupesh on 4/15/17.
//  Copyright © 2017 Mark Feaver. All rights reserved.
//

#import "CalendarViewController.h"
#import "NTMonthYearPicker.h"
#import "AddNewEventViewController.h"
#import "EventFeedViewController.h"
#import "UIImage+GKContact.h"
#import "UIImageView+WebCache.h"
#import "CustomCalendarEvenTableViewCell.h"
#import "EventDetail2ViewController.h"
#import "SocialNetwork-Swift.h"



static NSString *CALENDAR_CELL = @"customCalEventCellID";

@interface CalendarViewController () <UITableViewDelegate,UITableViewDataSource,AddNewEvent2ViewControllerDelegate>

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;
@property (strong, nonatomic) JTCalendarManager *calendarManager;
@property(strong,nonatomic) NSDate *dateSelected;
@property(strong,nonatomic) NSMutableDictionary *eventsByDate;
@property(weak,nonatomic) IBOutlet UITableView *tableEvent;
@property(weak,nonatomic) IBOutlet UIView *pickerView;
//@property(strong,nonatomic) NSMutableArray *allEvents;
@property(strong,nonatomic) NSArray *filteredEvents;
@property(strong,nonatomic) NSDateComponents *components;
@property(strong,nonatomic) NSDateFormatter *monthYearFormatter;
@property(strong,nonatomic) NSDateFormatter *timeRangeFormatter;
@property(strong,nonatomic) NSDateFormatter *dateFormatter;
@property(strong,nonatomic) NTMonthYearPicker *picker;
@property(strong,nonatomic)CLLocationManager *locationManager;
@property(strong,nonatomic)CLLocation *currentLocation;
@property(strong,nonatomic)UITapGestureRecognizer *gestureRecognizer;
@property(strong,nonatomic)UIView *navBarTapView;

@end

@implementation CalendarViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   //  [self getAllEvents];
    
    
    
    UINib *nibNamed = [UINib nibWithNibName:@"CustomCalendarEvenTableViewCell" bundle:nil];
    [self.tableEvent registerNib:nibNamed forCellReuseIdentifier:CALENDAR_CELL];
    
    
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    // Generate random events sort by date using a dateformatter for the demonstration
    //[self createRandomEvents];
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:[NSDate date]];
    
    _components = [[NSDateComponents alloc] init];
    
    //    NSLog(@"Next page loaded");
    
    [_calendarManager reload];
    
    _timeRangeFormatter = [NSDateFormatter new];
    _timeRangeFormatter.dateFormat = @"h:mm a";

        _monthYearFormatter = [NSDateFormatter new];
        _monthYearFormatter.dateFormat = @"MMMM yyyy";
    
    
    _dateFormatter = [NSDateFormatter new];
    _dateFormatter.dateFormat = @"MMMM dd, yyyy";


    NSString *monthYear = [_monthYearFormatter stringFromDate:[_calendarManager date]];
    //self.title = [NSString stringWithFormat:@"%@ ▼",monthYear];
    self.title = [NSString stringWithFormat:@"%@",monthYear];
    // Initialize the picker
    _picker = [[NTMonthYearPicker alloc] init];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    // Set mode to month + year
    // This is optional; default is month + year
    _picker.datePickerMode = NTMonthYearPickerModeMonthAndYear;
    
    // Set minimum date to January 2000
    // This is optional; default is no min date
    [comps setDay:1];
    [comps setMonth:1];
    [comps setYear:0];
    _picker.minimumDate = [cal dateFromComponents:comps];
    
    // Set maximum date to next month
    // This is optional; default is no max date
//    [comps setDay:0];
//    [comps setMonth:1];
//    [comps setYear:0];
//    _picker.maximumDate = [cal dateByAddingComponents:comps toDate:[NSDate date] options:0];
    
    // Set initial date to last month
    // This is optional; default is current month/year
    [comps setDay:0];
    [comps setMonth:-1];
    [comps setYear:0];
    _picker.date = [cal dateByAddingComponents:comps toDate:[NSDate date] options:0];
    
    [self loadLocation];

    //[self.navigationController setHidesBottomBarWhenPushed:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self downloadAllEvents];
}

-(void)downloadAllEvents{
    PFQuery *query = [PFQuery queryWithClassName:@"Event"];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (error == nil) {
            _eventsByDate = [NSMutableDictionary new];
            //  _allEvents = [NSMutableArray new];
            
            for(PFObject *event in objects)
            {
                //  [_allEvents  addObjectsFromArray: [eventDict allValues]];
                
                //  NSMutableDictionary *snap = [eventDict objectForKey:eventKey];
                // NSLog(@"---> %@",snap);
                // [snap setObject:userKey forKey:@"userkey"];
                //   [snap setObject:eventKey forKey:@"eventkey"];
                PFGeoPoint *geoPoint = [event objectForKey:@"location"];
                CLLocation *location = [[CLLocation alloc] initWithLatitude:geoPoint.latitude longitude:geoPoint.longitude];
                
                NSLog(@"latitude == %f == longigute == %f",geoPoint.latitude,geoPoint.longitude);
                NSString *title = (NSString *)[event objectForKey:@"title"];
                NSLog(@"Event-Name == %@",title);
                
                CLLocationDistance distance = [location distanceFromLocation:_currentLocation]/1609.344;
                
                //yasir commenting
                if(_currentLocation!=nil&&distance <= 50.0)
                {
                    NSDate *eventDate = [_dateFormatter dateFromString:[event objectForKey:@"date"]];
                    
                    // Use the date as key for eventsByDate
                    NSString *key = [[self dateFormatter] stringFromDate:eventDate];
                    if(key!=nil)
                    {
                        if(!_eventsByDate[key]){
                            _eventsByDate[key] = [NSMutableArray new];
                        }
                        [_eventsByDate[key] addObject:event];
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //Your main thread code goes in here
                [_calendarManager reload];
                [self showEventsForCurrentDate:_dateSelected?:_calendarManager.date];
            });
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    // recognise taps on navigation bar to hide
    _gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectMonthPicker:)];
    _gestureRecognizer.numberOfTapsRequired = 1;
    CGRect frame = CGRectMake(self.view.frame.size.width/4, 0, self.view.frame.size.width/2, 44);
    _navBarTapView = [[UIView alloc] initWithFrame:frame];
    [self.navigationController.navigationBar addSubview:_navBarTapView];
    _navBarTapView.backgroundColor = [UIColor clearColor];
    [_navBarTapView setUserInteractionEnabled:YES];
    [_navBarTapView addGestureRecognizer:_gestureRecognizer];

    
    
//        FIRDatabaseReference *databaseRef = [[FIRDatabase database] reference];
//         FIRUser *user = [FIRAuth auth].currentUser;
//        [[databaseRef child:@"events"]  observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//    
//             if (snapshot.value != [NSNull null]){
//                 
//                 _eventsByDate = [NSMutableDictionary new];
//                 _allEvents = [NSMutableArray new];
//
//                 for(NSString *userKey in [snapshot.value allKeys])
//                 {
//                     NSDictionary *eventDict = [snapshot.value objectForKey:userKey];
//                     for(NSString *eventKey in [eventDict allKeys])
//                     {
//                         [_allEvents  addObjectsFromArray: [eventDict allValues]];
//                         
//                         NSMutableDictionary *snap = [eventDict objectForKey:eventKey];
//                             NSLog(@"---> %@",snap);
//                         [snap setObject:userKey forKey:@"userkey"];
//                         [snap setObject:eventKey forKey:@"eventkey"];
//
//                             CLLocation *location = [[CLLocation alloc] initWithLatitude:[[snap objectForKey:@"latitude"] doubleValue] longitude:[[snap objectForKey:@"longitude"] doubleValue]];
//                             CLLocationDistance distance = [location distanceFromLocation:_currentLocation]/1609.344;
//                             
//                            if(_currentLocation!=nil&&distance <= 50.0)
//                            {
//                             NSDate *eventDate = [_dateFormatter dateFromString:[snap objectForKey:@"date"]];
//                             
//                             // Use the date as key for eventsByDate
//                             NSString *key = [[self dateFormatter] stringFromDate:eventDate];
//                             if(key!=nil)
//                             {
//                                 if(!_eventsByDate[key]){
//                                     _eventsByDate[key] = [NSMutableArray new];
//                                 }
//                                 
//                                 [_eventsByDate[key] addObject:snap];
//                             }
//                            }
//                         
//                     }
//                 }
//                 dispatch_async(dispatch_get_main_queue(), ^{
//                     //Your main thread code goes in here
//                     [_calendarManager reload];
//                     [self showEventsForCurrentDate:_dateSelected?:_calendarManager.date];
//                 });
//             }
//         }];
    
}
#pragma mark - CalendarManager delegate

// Exemple of implementation of prepareDayView method
// Used to customize the appearance of dayView
- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    // Today
    dayView.circleView.layer.cornerRadius=0.0;
    if([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor whiteColor];
        dayView.dotView.backgroundColor = [UIColor colorWithRed:80.0/255 green:210.0/255 blue:194.0/255 alpha:1.0];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    // Selected date
    else if(_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]){
        dayView.circleView.hidden = NO;
        dayView.circleView.backgroundColor = [UIColor whiteColor];
        dayView.dotView.backgroundColor = [UIColor colorWithRed:80.0/255 green:210.0/255 blue:194.0/255 alpha:1.0];
        dayView.textLabel.textColor = [UIColor blackColor];
    }
    // Other month
    else if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor lightGrayColor];
    }
    // Another day of the current month
    else{
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor whiteColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
    }
    
    if([self haveEventForDay:dayView.date]){
        dayView.dotView.hidden = NO;
    }
    else{
        dayView.dotView.hidden = YES;
    }
}

- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    _dateSelected = dayView.date;
    
    // Animation for the circleView
    dayView.circleView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.1, 0.1);
    [UIView transitionWithView:dayView
                      duration:.3
                       options:0
                    animations:^{
                        dayView.circleView.transform = CGAffineTransformIdentity;
                        [_calendarManager reload];
                    } completion:nil];
    
    
    // Don't change page in week mode because block the selection of days in first and last weeks of the month
    if(_calendarManager.settings.weekModeEnabled){
        return;
    }
    
    // Load the previous or next page if touch a day from another month
    
    if(![_calendarManager.dateHelper date:_calendarContentView.date isTheSameMonthThan:dayView.date]){
        if([_calendarContentView.date compare:dayView.date] == NSOrderedAscending){
            [_calendarContentView loadNextPageWithAnimation];
        }
        else{
            [_calendarContentView loadPreviousPageWithAnimation];
        }
    }
    
    [self showEventsForCurrentDate:_dateSelected];
}
// Used only to have a key for _eventsByDate
- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar
{
    NSString *monthYear = [_monthYearFormatter stringFromDate:[_calendarManager date]];
    self.title = [NSString stringWithFormat:@"%@ ▾",monthYear];
}

- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar
{
    NSString *monthYear = [_monthYearFormatter stringFromDate:[_calendarManager date]];
    self.title = [NSString stringWithFormat:@"%@ ▼",monthYear];

}

- (NSDateFormatter *)dateFormatter
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return dateFormatter;
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSString *key = [[self dateFormatter] stringFromDate:date];
    
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    
    return NO;
    
}

- (void)createRandomEvents
{
    _eventsByDate = [NSMutableDictionary new];
    
    for(int i = 0; i < 30; ++i){
        // Generate 30 random dates between now and 60 days later
        NSDate *randomDate = [NSDate dateWithTimeInterval:(rand() % (3600 * 24 * 60)) sinceDate:[NSDate date]];
        
        // Use the date as key for eventsByDate
        NSString *key = [[self dateFormatter] stringFromDate:randomDate];
        
        if(!_eventsByDate[key]){
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:randomDate];
    }
}
//-(void)getAllEvents
//{
//    _store = [[EKEventStore alloc] init];
//    
//    
//    // Get the appropriate calendar
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    
//    
//    if ([_store respondsToSelector:@selector(requestAccessToEntityType:completion:)])
//    {
//        /* iOS Settings > Privacy > Calendars > MY APP > ENABLE | DISABLE */
//        [_store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
//         {
//             if ( granted )
//             {
//                 NSLog(@"User has granted permission!");
//                 // Create the start date components
//                 NSDateComponents *oneDayAgoComponents = [[NSDateComponents alloc] init];
//                 oneDayAgoComponents.day = -1;
//                 NSDate *oneDayAgo = [calendar dateByAddingComponents:oneDayAgoComponents
//                                                               toDate:[NSDate date]
//                                                              options:0];
//                 
//                 // Create the end date components
//                 NSDateComponents *oneYearFromNowComponents = [[NSDateComponents alloc] init];
//                 oneYearFromNowComponents.year = 1;
//                 NSDate *oneYearFromNow = [calendar dateByAddingComponents:oneYearFromNowComponents
//                                                                    toDate:[NSDate date]
//                                                                   options:0];
//                 
//                 // Create the predicate from the event store's instance method
//                 NSPredicate *predicate = [_store predicateForEventsWithStartDate:oneDayAgo
//                                                                         endDate:oneYearFromNow
//                                                                       calendars:nil];
//                 
//                 // Fetch all events that match the predicate
//                 NSArray *events = [_store eventsMatchingPredicate:predicate];
//                 NSLog(@"The content of array is%@",events);
//                 _eventsByDate = [NSMutableDictionary new];
//                 _allEvents  = events;
//                 for(EKEvent *event in events)
//                 {
//                     
//                         NSDate *randomDate = event.startDate;
//                     
//                         // Use the date as key for eventsByDate
//                         NSString *key = [[self dateFormatter] stringFromDate:randomDate];
//                         
//                         if(!_eventsByDate[key]){
//                             _eventsByDate[key] = [NSMutableArray new];
//                         }
//                         
//                         [_eventsByDate[key] addObject:randomDate];
//                     
//
//                 }
//                 dispatch_async(dispatch_get_main_queue(), ^{
//                     //Your main thread code goes in here
//                     [_calendarManager reload];
//                     [self showEventsForCurrentDate:[NSDate date]];
//                 });
//             }
//             else
//             {
//                 NSLog(@"User has not granted permission!");
//             }
//         }];
//    }
//}


#pragma mark -
#pragma mark TableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80.0f;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _filteredEvents.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomCalendarEvenTableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:CALENDAR_CELL];
    PFObject *event = [_filteredEvents objectAtIndex:indexPath.row];
    
    PFFile *file = [event objectForKey:@"picture"];
    if ( file.url != nil ) {
        [cell1.imgCell sd_setImageWithURL:[NSURL URLWithString:file.url]];
    }else{
        [cell1.imgCell setImage:nil];
    }
    
    cell1.lblTitle.text = [event objectForKey:@"title"];
    
    cell1.lblSubtitle.text = [NSString stringWithFormat:@"%@, %@",[event objectForKey:@"date"],[event objectForKey:@"from"]];
    
    cell1.lblLocation.text = [event objectForKey:@"locationname"];
    
    
    PFGeoPoint *geoPoint = [event objectForKey:@"location"];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:geoPoint.latitude longitude:geoPoint.longitude];
    CLLocationDistance distance = [location distanceFromLocation:_currentLocation]/1609.344;
    
    cell1.lblMiles.text = [NSString stringWithFormat:@"%.1f mi",distance];
    
    
//    FIRStorageReference *storageRef = [[[FIRStorage storage] reference] child:[event objectForKey:@"photopath"]];
//    [storageRef dataWithMaxSize:1 * 512 * 512 completion:^(NSData * _Nullable data, NSError * _Nullable error) {
//        if(data==nil)
//            imageView.image = [UIImage imageForName:[[title.text componentsSeparatedByString:@" "] firstObject] size:imageView.frame.size];
//        else
//        {
//        UIImage *image = [UIImage imageWithData:data];
//        imageView.image = image;
//        }
//        
//    }];

    return cell1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSDictionary *event = [_filteredEvents objectAtIndex:indexPath.row];
//    NSString *website=[event objectForKey:@"website"];
//    if(website!=nil)
//    {
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:website]];
//    }
    
    
    
    
    
    EventDetail2ViewController *edVC = [[EventDetail2ViewController alloc] init];
    
    edVC.event = [_filteredEvents objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:edVC animated:YES];
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    /*
    EventFeedViewController *eventFeedViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"eventFeedViewController"];
    eventFeedViewController.event = [_filteredEvents objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:eventFeedViewController animated:YES];
    */
    

}
-(void)showEventsForCurrentDate:(NSDate*)date
{

    NSString *key = [[self dateFormatter] stringFromDate:date];
    _filteredEvents = _eventsByDate[key];

   /* _components.day = [[NSCalendar currentCalendar] ordinalityOfUnit:(NSCalendarUnitDay) inUnit:(NSCalendarUnitEra) forDate:date];
    NSDate *dayBegin = [[NSCalendar currentCalendar] dateFromComponents:_components];
    _components.day += 1;
    NSDate *dayEnd = [[NSCalendar currentCalendar] dateFromComponents:_components];
    // Create the predicate from the event store's instance method
    NSPredicate *predicate = [_store predicateForEventsWithStartDate:dayBegin
                                                             endDate:dayEnd
                                                           calendars:nil];
    
    // Fetch all events that match the predicate
    _filteredEvents = [_store eventsMatchingPredicate:predicate];*/
    [self.tableEvent reloadData];

}
-(IBAction)selectMonthPicker:(id)sender
{
    
    _pickerView.hidden=NO;

    CGSize pickerSize = [_picker sizeThatFits:CGSizeZero];
    // iPhone: show picker at the bottom of the screen
    if( ![_picker isDescendantOfView:self.view] ) {
        _picker.frame = CGRectMake((_pickerView.frame.size.width - pickerSize.width)/2 , 44, pickerSize.width, pickerSize.height );
        [self.pickerView addSubview:_picker];
    }

}
-(IBAction)donePicker:(id)sender
{
    _pickerView.hidden=YES;
    NSString *monthYear = [_monthYearFormatter stringFromDate:_picker.date];
    self.title = [NSString stringWithFormat:@"%@ ▼",monthYear];
    [_calendarManager setDate:_picker.date];
    [_calendarManager reload];

}
-(IBAction)camcelPicker:(id)sender
{
    _pickerView.hidden=YES;

}


-(void)loadLocation
{
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
    {
        [_locationManager requestAlwaysAuthorization];
    }
    [_locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    _currentLocation = newLocation;
    
    NSLog(@"Calendar Has Location == %f",self.currentLocation.coordinate.latitude);
    NSLog(@"Calendar Has Location == %f",self.currentLocation.coordinate.longitude);
    
    
    [manager stopUpdatingLocation];
}

-(IBAction)addNew:(id)sender
{
    
    NSLog(@"addNew Pressed");
    
    
    
    AddNewEvent2ViewController *event2 = [self.storyboard instantiateViewControllerWithIdentifier:@"AddNewEvent2ViewControllerID"];
    event2.modalPresentationStyle = UIModalPresentationOverFullScreen;
    event2.userLocation = self.currentLocation;
    event2.delegate = self;
    [self presentViewController:event2 animated:YES completion:nil];
    
    /*
    AddNewEventViewController *addNewEventViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"addNewEventViewController"];
   [self.navigationController pushViewController:addNewEventViewController animated:YES];
    */
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_navBarTapView removeGestureRecognizer:_gestureRecognizer];
    [_navBarTapView removeFromSuperview];
 
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)AddNewEvent2ViewControllerDidPressCloseButton{
    NSLog(@"AddNewEvent2ViewControllerDidPressCloseButton");
    
    [self downloadAllEvents];
}

@end
