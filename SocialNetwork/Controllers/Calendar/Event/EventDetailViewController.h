//
//  EventDetailViewController.h
//  eventmanagement
//
//  Created by Nick Schulze on 2/21/15.
//  Copyright (c) 2015 infusion infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>
//#import "UIColor+Snagget.h"
//#import "ParseAccessor.h"
#import "EventDetailView.h"
//#import "EventDetailCommentViewController.h"

@class EventDetailViewController;
@protocol EventDetailsDelegate <NSObject>

-(void)editEvent:(PFObject*)event;

@optional
@required

@end

@interface EventDetailViewController : UIViewController <UITextFieldDelegate, UIScrollViewDelegate, MKMapViewDelegate, UITextViewDelegate, UIImagePickerControllerDelegate, EventDetailDelegate>
{
    float width;
    float height;
    
    UIScrollView* background;
    MKMapView* map;
    UIButton* back;
    UIButton* post;
    
    UIImageView* avatar;
    UILabel* title;
    UIView* headerView;
    UILabel* headerTitle;
    UILabel* name;
    UITextView* description;
    UIImageView* locationPointer;
    UILabel* location;
    UIButton* locationButton;
    UILabel* address;
    UILabel* time;
    UITextView* url;
    UILabel* letter;
    CLLocationCoordinate2D locationPoint;
    PFGeoPoint* userPoint;
    
    UIScrollView* posts;
    int shift;
    UIImagePickerController *imagePicker;

    UIView* addPost;
    UIView* postLine;
    UITextView* postText;
    UIButton* cancel;
    UILabel* postTitle;
    UIButton* makePost;
    UIButton* photoButton;
    UIView* libraryorcamera;
    UIButton* closelibraryorcamera;
    UIButton* closePopout;
    UIButton* camera;
    UIButton* library;
    
   // ParseAccessor* parse;
    PFObject* object;
    UIImage* image;
    UIImageView* photo;
    //EventDetailCommentViewController* comments;
    
    UIView* imageBackground;
    UIImageView* popoutImage;
    BOOL refreshing;
    UILabel* pullRefresh;
    UIButton* editButton;
    int items;
}

//@property (nonatomic, retain) ParseAccessor* parse;
@property (nonatomic, retain) id <EventDetailsDelegate> delegate;

-(void)setDetail:(PFObject*)object;

-(void)setPosts:(NSArray*)postArray;

-(void)addPosts:(NSArray*)postArray;

-(void)setComment:(PFObject*)object;

-(void)setComments:(NSArray*)postArray;

-(void)moveOut;

-(void)moveIn;

-(void)moveOutAddPost;

-(void)moveInAddPost;

-(void)setUserPoint:(PFGeoPoint*)center;
@end
