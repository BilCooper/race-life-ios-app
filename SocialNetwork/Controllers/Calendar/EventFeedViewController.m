//
//  EventFeedViewController.m
//  Do-Invision
//
//  Created by Rupesh on 7/2/17.
//  Copyright © 2017 Mark Feaver. All rights reserved.
//

#import "EventFeedViewController.h"
#import "AddFeedViewController.h"
#import "EventMapViewController.h"
#import "UIImage+GKContact.h"
#import "UIImageView+WebCache.h"
//@import FirebaseDatabase;
//@import FirebaseStorage;


@interface EventFeedViewController ()
@property(weak,nonatomic) IBOutlet UITableView *tableEventFeed;
@property(weak,nonatomic) IBOutlet UILabel *lblEventName;
@property(weak,nonatomic) IBOutlet UILabel *lblLocationName;
@property(weak,nonatomic) IBOutlet UIImageView *imageView;
@property(weak,nonatomic) IBOutlet UILabel *lblDateAndTime;
@property(weak,nonatomic) IBOutlet UILabel *lblDescription;
@property(weak,nonatomic) IBOutlet UILabel *lblWebsite;
@property(nonatomic,strong)NSURL *fileUrl;
@property(nonatomic,strong)NSMutableArray *eventFeeds;

@end

@implementation EventFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblEventName.text=[self.event objectForKey:@"title"];
    self.lblLocationName.text=[self.event objectForKey:@"locationname"];
    self.lblDateAndTime.text=[NSString stringWithFormat:@"Begins at %@",[self.event objectForKey:@"from"]];
    self.lblDescription.text=[self.event objectForKey:@"description"];
    self.lblWebsite.text=[self.event objectForKey:@"website"];
    self.imageView.layer.borderWidth=1.0;
    self.imageView.layer.borderColor = [UIColor colorWithRed:19.0/255 green:168.0/255 blue:158.0/255 alpha:1.0].CGColor;
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    self.imageView.image = [UIImage imageForName:[[self.lblEventName.text componentsSeparatedByString:@" "] firstObject] size:self.imageView.frame.size];
    PFFile *file = [self.event objectForKey:@"picture"];
    if(file.url!=nil)
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:file.url]];

    self.eventFeeds = [[NSMutableArray alloc] init];
//    FIRStorageReference *storageRef = [[[FIRStorage storage] reference] child:[self.event objectForKey:@"photopath"]];
//    [storageRef dataWithMaxSize:1 * 512 * 512 completion:^(NSData * _Nullable data, NSError * _Nullable error) {
//        if(data==nil)
//        self.imageView.image = [UIImage imageForName:[[self.lblEventName.text componentsSeparatedByString:@" "] firstObject] size:self.imageView.frame.size];
//        else
//        {
//          UIImage *image = [UIImage imageWithData:data];
//          self.imageView.image = image;
//        }
//        
//    }];

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    PFQuery *query = [PFQuery queryWithClassName:@"EventFeed"];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (error == nil) {
            [self.eventFeeds removeAllObjects];
            [self.eventFeeds addObjectsFromArray:objects];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            //Your main thread code goes in here
            [self.tableEventFeed reloadData];
        });
    }];

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.eventFeeds count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *eventFeed = [self.eventFeeds objectAtIndex:[self.eventFeeds count]-indexPath.row-1];
    UITableViewCell *cell ;
    PFUser *user = [eventFeed objectForKey:@"user"];
      [user fetchIfNeeded];
    if([eventFeed objectForKey:@"picture"])
    {
        cell =[tableView dequeueReusableCellWithIdentifier:@"cell2"];
        UILabel *username = [cell.contentView viewWithTag:1];
        UILabel *time = [cell.contentView viewWithTag:2];
        UILabel *comment = [cell.contentView viewWithTag:3];
        UIImageView *imageView = [cell.contentView viewWithTag:4];
        if(user.isDataAvailable)
            username.text = [user objectForKey:@"displayName"];
        else
            username.text = @"";
    
        time.text = [eventFeed objectForKey:@"time"];
        comment.text = [eventFeed objectForKey:@"comment"];
        PFFile *file = [eventFeed objectForKey:@"picture"];
        if(file.url!=nil)
            [imageView sd_setImageWithURL:[NSURL URLWithString:file.url]];

//        FIRStorageReference *storageRef = [[[FIRStorage storage] reference] child:[eventFeed objectForKey:@"photopath"]];
//        [storageRef dataWithMaxSize:1 * 512 * 512 completion:^(NSData * _Nullable data, NSError * _Nullable error) {
//            UIImage *image = [UIImage imageWithData:data];
//            imageView.image = image;
//            
//        }];

    }
    else
    {
       cell =[tableView dequeueReusableCellWithIdentifier:@"cell1"];
    
    UILabel *username = [cell.contentView viewWithTag:1];
    UILabel *time = [cell.contentView viewWithTag:2];
    UILabel *comment = [cell.contentView viewWithTag:3];
    if(user.isDataAvailable)
    username.text = [user objectForKey:@"displayName"];
    else
        username.text = @"";

    time.text = [eventFeed objectForKey:@"time"];
    comment.text = [eventFeed objectForKey:@"comment"];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *eventFeed = [self.eventFeeds objectAtIndex:[self.eventFeeds count]-indexPath.row-1];
    CGSize constraint = CGSizeMake([[UIApplication sharedApplication] keyWindow].frame.size.width-140, CGFLOAT_MAX);
    CGSize size;
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [[eventFeed objectForKey:@"comment"]  boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Avenir Medium" size:13.0]}
                                                  context:context].size;

    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));

    if([eventFeed objectForKey:@"picture"]!=nil)
        return MAX(150.0, size.height+125);
    else
       return MAX(55.0, size.height+10);

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *eventFeed = [self.eventFeeds objectAtIndex:[self.eventFeeds count]-indexPath.row-1];


//    FIRStorageReference *storageRef = [[[FIRStorage storage] reference] child:[eventFeed objectForKey:@"photopath"]];
//    [storageRef dataWithMaxSize:1 * 1024 * 1024 completion:^(NSData * _Nullable data, NSError * _Nullable error) {
      //  UIImage *image = [UIImage imageWithData:data];
    PFFile *file = [eventFeed objectForKey:@"picture"];
   NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:file.url]];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docPath_ = [paths objectAtIndex:0];
        NSString *filePath = [ docPath_ stringByAppendingPathComponent:@"Event Feed.jpg"];
         [data writeToFile:filePath atomically:YES];
        self.fileUrl = [NSURL fileURLWithPath:filePath];
        QLPreviewController *previewController = [[QLPreviewController alloc] init];
        previewController.dataSource = self;
        previewController.delegate = self;
        // start previewing the document at the current section index
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self.navigationController pushViewController:previewController animated:YES];
            
        }];
   // }];

}
- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller
{
    return 1;
}

- (id)previewController:(QLPreviewController *)previewController previewItemAtIndex:(NSInteger)idx {
    return self.fileUrl;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(scrollView.contentOffset.y>175)
        self.title =[self.event objectForKey:@"title"];
    else
        self.title = nil;
    
    
}
-(IBAction)addNew:(id)sender
{
    
    NSArray *items = @[
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"photo-camera"] title:@"Take Photo"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"picture-choose"] title:@"Choose Photo"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"editText"] title:@"Text"],
                       ];
    
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:items];
    av.delegate = self;
    //    av.bounces = NO;
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        AddFeedViewController *addFeedViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"addFeedViewController"];
        addFeedViewController.event = self.event;
        addFeedViewController.image = chosenImage;
        [self.navigationController pushViewController:addFeedViewController animated:YES];

    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];

}
#pragma mark - RNGridMenuDelegate

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex {
    NSLog(@"Dismissed with item %d: %@", itemIndex, item.title);
    if(itemIndex==0)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else if (itemIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        [self presentViewController:picker animated:YES completion:nil];

    }
    else
    {
        AddFeedViewController *addFeedViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"addFeedViewController"];
        addFeedViewController.event = self.event;
        [self.navigationController pushViewController:addFeedViewController animated:YES];
    }

}

-(IBAction)openMap:(id)sender
{
    EventMapViewController *eventMapViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"eventMapViewController"];
    eventMapViewController.event = self.event;
    [self.navigationController pushViewController:eventMapViewController animated:YES];

}

-(IBAction)openWebsite:(id)sender
{
        NSString *website=[self.event objectForKey:@"website"];
        if(website!=nil)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:website]];
        }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
