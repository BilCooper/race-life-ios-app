//
//  EventMapViewController.m
//  Do-Invision
//
//  Created by Rupesh on 7/17/17.
//  Copyright © 2017 Mark Feaver. All rights reserved.
//

#import "EventMapViewController.h"
@import MapKit;
@interface EventMapViewController ()
@property (nonatomic,weak)IBOutlet MKMapView *mapView;
@end

@implementation EventMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title =[self.event objectForKey:@"title"];
    CLLocationCoordinate2D location;
    MKCoordinateSpan span;
    PFGeoPoint *geoPoint = [self.event objectForKey:@"location"];
    location.latitude = geoPoint.latitude;
    location.longitude =geoPoint.longitude;
    span.latitudeDelta=0.2;
    span.longitudeDelta=0.2;
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = location;
    [self.mapView addAnnotation:annotation];
    
    MKCoordinateRegion region;
    region.center = location;
    region.span = span;
    [self.mapView setRegion:region animated:NO];
    
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:[annotation coordinate] addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:[self.event objectForKey:@"title"]];
    
    // Set the directions mode to "Driving"
    // Can use MKLaunchOptionsDirectionsModeDriving instead
    NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
    
    // Get the "Current User Location" MKMapItem
    MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
    
    // Pass the current location and destination map items to the Maps app
    // Set the direction mode in the launchOptions dictionary
    [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem] launchOptions:launchOptions];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
