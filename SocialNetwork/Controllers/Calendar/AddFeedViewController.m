//
//  AddFeedViewController.m
//  Do-Invision
//
//  Created by Rupesh on 7/2/17.
//  Copyright © 2017 Mark Feaver. All rights reserved.
//

#import "AddFeedViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
//@import FirebaseDatabase;
//@import FirebaseStorage;
//@import FirebaseAuth;


@interface AddFeedViewController ()
@property(nonatomic,weak)IBOutlet UITextView *textView;
@property(nonatomic,weak)IBOutlet UIImageView *imageView;
@property(nonatomic,weak)IBOutlet NSLayoutConstraint *imageHeightConstraint;
//@property(nonatomic,strong)NSString *username;

@end

@implementation AddFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(_image)
        self.imageView.image = _image;
    else
        self.imageHeightConstraint.constant=0;
    
//    FIRUser *user = [FIRAuth auth].currentUser;
//    FIRDatabaseReference *databaseRef = [[FIRDatabase database] reference];
//    [[[databaseRef child:@"users"] child:user.uid] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//        
//        if (snapshot.value != [NSNull null]){
//            
//            NSDictionary *snap= snapshot.value;
//            dispatch_async(dispatch_get_main_queue(), ^{
//                //Your main thread code goes in here
//                NSArray *tempArray = [[snap objectForKey:@"name"] componentsSeparatedByString:@" "];
//                NSString *firstPart = [tempArray firstObject];
//                NSString *secondPart;
//                if([tempArray count]>1&&[[tempArray objectAtIndex:1] length]>0)
//                secondPart = [[tempArray objectAtIndex:1] substringToIndex:1];
//                
//                self.username=[NSString stringWithFormat:@"%@ %@.",firstPart,secondPart];
//            });
//        }
//    }];
//
    self.textView.layer.borderWidth = 1.0;
    self.textView.layer.borderColor = [UIColor lightGrayColor].CGColor;

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
}

-(IBAction)addNew:(id)sender
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"h:mma"];
    NSString *dateString = [dateFormat stringFromDate:[NSDate date]];

    PFObject *eventFeed = [PFObject objectWithClassName:@"EventFeed"];
    [eventFeed setObject:[PFUser currentUser] forKey:@"user"];
    [eventFeed setObject:self.event forKey:@"event"];
    [eventFeed setObject:self.textView.text?:@"" forKey:@"comment"];
    [eventFeed setObject:dateString forKey:@"time"];
    if(_image!=nil)
    {
        PFFile *picture = [PFFile fileWithData:UIImageJPEGRepresentation(_image, 0.5)];
        [eventFeed setObject:picture forKey:@"picture"];
    }
    [eventFeed saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        
    }];

//    NSMutableArray *feeds = [[self.event objectForKey:@"feed"] count]>0?[[self.event objectForKey:@"feed"] mutableCopy]:[[NSMutableArray alloc] init];
//    NSString *photoPath = @"";
//    
//    if(_image!=nil)
//    {
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        FIRUser *user = [FIRAuth auth].currentUser;
//
//        photoPath = [NSString stringWithFormat:@"images/%@/%@.jpg",user.uid,[NSDate date]];
//        FIRStorageReference *storageRef = [[[FIRStorage storage] reference] child:photoPath];
//        
//        [storageRef putData:UIImageJPEGRepresentation(_image, 0.5)
//                   metadata:nil
//                 completion:^(FIRStorageMetadata *metadata,
//                              NSError *error) {
//                     if (error != nil) {
//                         // Uh-oh, an error occurred!
//                     } else {
//                         // Metadata contains file metadata such as size, content-type, and download URL.
//                         // NSURL *downloadURL = metadata.downloadURL;
//                     }
//                     [MBProgressHUD hideHUDForView:self.view animated:YES];
//                     [self.navigationController popViewControllerAnimated:YES];
//
//                 }];
//    }
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"h:mma"];
//    NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
//
//    [feeds addObject:@{@"username": self.username,@"time":dateString,@"comment":self.textView.text,@"photopath":photoPath}];
//    [self.event setValue:feeds forKey:@"feed"];
//    FIRDatabaseReference *databaseRef = [[FIRDatabase database] reference];
//    [[[[[databaseRef child:@"events"] child:[self.event objectForKey:@"userkey"]] child:[self.event objectForKey:@"eventkey"]] child:@"feed"] setValue:feeds];
    //if(_image==nil)
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
