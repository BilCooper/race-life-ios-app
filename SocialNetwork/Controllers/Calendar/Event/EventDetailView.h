//
//  EventDetailView.h
//  eventmanagement
//
//  Created by Nick Schulze on 2/22/15.
//  Copyright (c) 2015 infusion infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "UIColor+Snagget.h"
//#import "ParseAccessor.h"

@class EventDetailView;
@protocol EventDetailDelegate <NSObject>
-(void)goToEventComments:(PFObject*)event;
-(void)imageTap:(UIImage*)img;
@optional
@required

@end

@interface EventDetailView : UIView
{
    UILabel* userName;
    UILabel* time;
    UITextView* comment;
    UIImageView* image;
    PFObject* object;
    PFGeoPoint* userLocation;
    //ParseAccessor* parse;
    
    UIButton* button;
    UIButton* imagePopout;
    float width;
    float height;
    int shift;
}

//@property (nonatomic, retain) ParseAccessor* parse;
@property (nonatomic, retain) PFGeoPoint* userLocation;
@property (nonatomic, retain) id<EventDetailDelegate> delegate;

-(id)init:(PFObject*)_object withShift:(int)_shift andLine:(BOOL)lineSwitch;

-(int)getShift;

-(void)userProfile;

@end
