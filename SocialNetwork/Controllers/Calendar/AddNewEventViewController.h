//
//  AddNewEventViewController.h
//  Do-Invision
//
//  Created by Rupesh on 4/19/17.
//  Copyright © 2017 Mark Feaver. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewEventViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

-(void)locationSelected:(NSString*)location withLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude;

@end
