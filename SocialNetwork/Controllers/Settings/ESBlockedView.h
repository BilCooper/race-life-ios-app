

/**
 *  Interface declaration of the blocked view, showing all the blocked contacts
 */
@interface ESBlockedView : UITableViewController <UIActionSheetDelegate>

@end
