//
//  HelperParse.h
//  SocialNetwork
//
//  Created by Yasir on 1/14/18.
//  Copyright © 2018 Sugarsage. All rights reserved.
//
#import "UIKit/UIKit.h"
#import <Foundation/Foundation.h>

@interface HelperParse : NSObject


+ (instancetype)sharedInstance;
- (BOOL)image:(UIImage *)image1 isEqualTo:(UIImage *)image2;
-(void)saveParseObjectWithPara:(NSDictionary *)param withHandler:(void(^)(NSString *status))handler;

@end
