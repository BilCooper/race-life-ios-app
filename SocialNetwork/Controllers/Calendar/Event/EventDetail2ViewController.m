//
//  EventDetail2ViewController.m
//  SocialNetwork
//
//  Created by Yasir on 12/23/17.
//  Copyright © 2017 Sugarsage. All rights reserved.
//

#import "EventDetail2ViewController.h"
#import <MapKit/MapKit.h>
#import "UIImageView+WebCache.h"
#import "AddFeedViewController.h"

//[UIFont fontWithName:@"HelveticaNeue" size:15]

@interface EventDetail2ViewController ()<UIScrollViewDelegate,MKMapViewDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,UINavigationControllerDelegate,RNGridMenuDelegate,UITableViewDelegate,UITableViewDataSource>{
    
    CGFloat width;
    CGFloat height;
    
    UIScrollView * background;
    MKMapView *map;
    
    UIButton *back;
    UIButton *post;
    
    UIImageView *avatar;
    UILabel *title;
    UIView *headerView;
    
    UILabel *headerTitle;
    
    UILabel *time;
    UIImageView *locationPointer;
    
    UILabel *location;
    
    UIButton *locationButton;
    
    UITextView *description;
    UITextView *url;
    UITextView *postText;
    
    
    UILabel *address;
    

    UILabel *letter;
    
    //UIScrollView *posts;
    //UIView *addPost;
    //UIView *postLine;
    //UIButton *cancel;
    
    UIView *line;
    
    UIView *libraryorcamera;
    UIButton *closelibraryorcamera;
    UIButton *camera;
    UIButton *library;
    
    UIImagePickerController *imagePicker;
    
    UIButton *photoButton;
    
    UIImage *image;
    
    UIButton *makePost;
    
    PFObject* object;
    
    UITableView *tableSubPosts;
    NSMutableArray *eventFeeds;
    
}

@end

@implementation EventDetail2ViewController


-(id)init{
    if (self == [super init]) {
        
        UIScreen* main = [UIScreen mainScreen];
        width = main.bounds.size.width;
        height = main.bounds.size.height;
        
        [self.view setBackgroundColor:[UIColor lightGrayColor]];
        
        background = [[UIScrollView  alloc] initWithFrame:CGRectMake(0, 0, width, 320)];
        background.backgroundColor = [UIColor whiteColor];
        background.contentSize = CGSizeMake(width, height+136);
        [background setShowsVerticalScrollIndicator:NO];
        background.delegate = self;
        background.scrollEnabled = NO;
        background.tag = 1;
        [self.view addSubview:background];
        
        
        tableSubPosts = [[UITableView alloc] initWithFrame:CGRectMake(0, 320, width, height-320) style:UITableViewStylePlain];
        tableSubPosts.delegate = self;
        tableSubPosts.dataSource = self;
        tableSubPosts.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableSubPosts.backgroundColor = [UIColor clearColor];
        
        UINib *cell1Nib = [UINib nibWithNibName:@"Cell1TableViewCell" bundle:nil];
        UINib *cell2Nib = [UINib nibWithNibName:@"Cell2TableViewCell" bundle:nil];
        
        [tableSubPosts registerNib:cell1Nib forCellReuseIdentifier:@"cell1"];
        [tableSubPosts registerNib:cell2Nib forCellReuseIdentifier:@"cell2"];
        
        [self.view addSubview:tableSubPosts];
        
        eventFeeds = [[NSMutableArray alloc] init];
        
        
        UIView* borderRight = [[UIView alloc] initWithFrame:CGRectMake(width, 0, 1, height)];
        borderRight.backgroundColor = [UIColor blackColor];
        [background addSubview:borderRight];
        
        
        PFGeoPoint *checkpoint = [self.event objectForKey:@"location"];
        
        map = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, width, 88)];
        [map setMapType:MKMapTypeStandard];
        //[map setCenterCoordinate:CLLocationCoordinate2DMake(48, -93)];
        [map setCenterCoordinate:CLLocationCoordinate2DMake(checkpoint.latitude, checkpoint.longitude)];
        double scalingFactor = ABS( (cos(2 * M_PI * 48.0 / 360.0) ));
        MKCoordinateSpan span;
        span.latitudeDelta = .15/69.0;
        span.longitudeDelta = .15/(scalingFactor * 69.0);
        //[map setRegion:MKCoordinateRegionMake(CLLocationCoordinate2DMake(41.376+.0005, -93.559), span)];
        [map setRegion:MKCoordinateRegionMake(CLLocationCoordinate2DMake(checkpoint.latitude+.0005, checkpoint.longitude), span)];
        [map setUserInteractionEnabled:NO];
        [map setDelegate:self];
        [self.view addSubview:map];
        
        
        /*
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        //[annotation setCoordinate:CLLocationCoordinate2DMake(41.376, -93.559)];
        [annotation setCoordinate:CLLocationCoordinate2DMake(checkpoint.latitude, checkpoint.longitude)];
        [map addAnnotation:annotation];
        */
        
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        
        
        back = [[UIButton alloc] initWithFrame:CGRectMake(0, 4, 64, 64)];
        back.backgroundColor = [UIColor clearColor];
        [back addTarget:self action:@selector(goBackVC) forControlEvents:UIControlEventTouchUpInside];
        [back setImage:[UIImage imageNamed:@"left-carrot"] forState:UIControlStateNormal];
        [self.view addSubview:back];
        
        
        
        post = [[UIButton alloc] initWithFrame:CGRectMake(width-40, 22, 22, 22)];
        post.backgroundColor = [UIColor clearColor];
        [post setImage:[UIImage imageNamed:@"editBtn"] forState:UIControlStateNormal];
        [post addTarget:self action:@selector(showGridMenu) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:post];
        
        
        
        avatar = [[UIImageView alloc] initWithFrame:CGRectMake(10, 68, 88, 88)];
        avatar.backgroundColor = [UIColor whiteColor];
        avatar.layer.cornerRadius = 44;
        avatar.clipsToBounds = YES;
        avatar.contentMode = UIViewContentModeScaleAspectFill;
        [self.view addSubview:avatar];
        
        
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(108, 70, width-118, 60)];
        //title.textColor = [UIColor blackColor];
        title.textColor = [UIColor colorWithRed:113/255 green:113/255 blue:113/255 alpha:1.0];
        title.backgroundColor = [UIColor clearColor];
        title.numberOfLines = 2;
        [title setFont:[UIFont fontWithName:@"HelveticaNeue" size:20]];
        [background addSubview:title];
        
        
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 28, width, 60)];
        headerView.backgroundColor = [UIColor whiteColor];
        headerView.alpha = 0.0;
        [map addSubview:headerView];
     
        
        headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 46, width-40, 40)];
        headerTitle.textColor = [UIColor redColor];
        headerTitle.backgroundColor = [UIColor clearColor];
        headerTitle.textAlignment = NSTextAlignmentCenter;
        [headerTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:18]];
        headerTitle.alpha = 0.0;
        [map addSubview:headerTitle];
        
        
        time = [[UILabel alloc] initWithFrame:CGRectMake(20, 160, width-20, 30)];
        time.text = @"";
        //time.textColor = [UIColor blackColor];
        title.textColor = [UIColor colorWithRed:109/255 green:109/255 blue:109/255 alpha:1.0];
        time.backgroundColor = [UIColor clearColor];
        [time setFont:[UIFont fontWithName:@"HelveticaNeue" size:15.0]];
        [background addSubview:time];
        
        
        locationPointer = [[UIImageView alloc] initWithFrame:CGRectMake(15, 185, 20, 20)];
        locationPointer.backgroundColor = [UIColor clearColor];
        [locationPointer setImage:[UIImage imageNamed:@"pinRed"]];
        [background addSubview:locationPointer];
        
        
        location = [[UILabel alloc] initWithFrame:CGRectMake(50, 185, width-40, 20)];
        location.text = @"";
        [location setTextAlignment:NSTextAlignmentLeft];
        location.textColor = [UIColor colorWithRed:83.0/255.0 green:152.0/255.0 blue:255.0/255.0 alpha:1.0];
        location.backgroundColor = [UIColor clearColor];
        [location setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0]];
        [background addSubview:location];
        
        
        locationButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 185, width-20, 30)];
        locationButton.backgroundColor = [UIColor clearColor];
        //locationButton.alpha = 0.5;
        [locationButton addTarget:self action:@selector(goToAppleMap) forControlEvents:UIControlEventTouchUpInside];
        [background addSubview:locationButton];
        
        
        description = [[UITextView alloc] initWithFrame:CGRectMake(15, 215, width-20, 64)];
        [description setUserInteractionEnabled:NO];
        description.text = @"";
        description.textColor = [UIColor blackColor];
        description.backgroundColor = [UIColor whiteColor];
        [description setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0]];
        [background addSubview:description];
        
        
        address = [[UILabel alloc] initWithFrame:CGRectMake(10, 160, 120, 40)];
        address.text = @"";
        address.backgroundColor = [UIColor redColor];
        
        
        url = [[UITextView alloc] initWithFrame:CGRectMake(15, 265, (width-30), 40)];
        url.text = @"";
        [url setTextAlignment:NSTextAlignmentLeft];
        url.textColor = [UIColor colorWithRed:135/255 green:181/255 blue:228/255 alpha:1.0];
        url.backgroundColor = [UIColor clearColor];
        url.clipsToBounds = YES;
        [url setFont:[UIFont fontWithName:@"HelveticaNeue" size:11.0]];
        url.editable = NO;
        url.dataDetectorTypes = UIDataDetectorTypeLink;
        [background addSubview:url];
        
        
        letter = [[UILabel alloc]initWithFrame:CGRectMake(3, 6, 80, 78)];
        letter.textColor = [UIColor blueColor];
        [letter setTextAlignment:NSTextAlignmentCenter];
        [letter setFont:[UIFont fontWithName:@"HelveticaNeue" size:60.0]];
        [avatar addSubview:letter];
        
        line = [[UIView alloc] initWithFrame:CGRectMake(0, 299, width, 1)];
        line.backgroundColor = [UIColor colorWithRed:116.0/255.0 green:9.0/255.0 blue:36.0/255.0 alpha:1.0];
        [background addSubview:line];
        
        /*
        posts = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 300, width, height-60)];
        posts.contentSize = CGSizeMake(width, height-60);
        [posts setBackgroundColor:[UIColor lightGrayColor]];
        posts.delegate = self;
        posts.tag = 2;
        [background addSubview:posts];
        
        
        addPost = [[UIView alloc] initWithFrame:CGRectMake(0, height+224, width, height)];
        addPost.backgroundColor = [UIColor redColor];
        //addPost.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:addPost];
        
        cancel = [[UIButton alloc] initWithFrame:CGRectMake(10, 20, 80, 44)];
        cancel.backgroundColor = [UIColor clearColor];
        [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
        cancel.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [cancel.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:18.0]];
        //[cancel setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [cancel setTitleColor:[UIColor colorWithRed:166.0/255.0 green:9.0/255.0 blue:36.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [cancel addTarget:self action:@selector(moveOutAddPost) forControlEvents:UIControlEventTouchUpInside];
        [addPost addSubview:cancel];
        
        
        
        makePost = [[UIButton alloc] initWithFrame:CGRectMake(width-90, 20, 80, 44)];
        //makePost.backgroundColor = [UIColor blueColor];
        makePost.backgroundColor = [UIColor clearColor];
        [makePost setTitle:@"Add" forState:UIControlStateNormal];
        makePost.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [makePost.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:18.0]];
        [makePost setTitleColor:[UIColor colorWithRed:166.0/255.0 green:9.0/255.0 blue:36.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [makePost addTarget:self action:@selector(createNewPost) forControlEvents:UIControlEventTouchUpInside];
        [addPost addSubview:makePost];
        
        
        
        photoButton = [[UIButton alloc] initWithFrame:CGRectMake(10, height-319, 60, 60)];
        //photoButton.backgroundColor = [UIColor blueColor];
        photoButton.backgroundColor = [UIColor clearColor];
        [photoButton setImage:[UIImage imageNamed:@"placeholderImg"] forState:UIControlStateNormal];
        photoButton.clipsToBounds = YES;
        [photoButton addTarget:self action:@selector(photolibrarypicker) forControlEvents:UIControlEventTouchUpInside];
        [addPost addSubview:photoButton];
        
        
        postLine = [[UIView alloc] initWithFrame:CGRectMake(0, 64, width, 1)];
        //postLine.backgroundColor = [UIColor redColor];
        postLine.backgroundColor = [UIColor colorWithRed:166.0/255.0 green:9.0/255.0 blue:36.0/255.0 alpha:1.0];
        [addPost addSubview:postLine];
        
        
        postText = [[UITextView alloc] initWithFrame:CGRectMake(10, 74, width-20, 190)];
        [postText setFont:[UIFont fontWithName:@"HelveticaNeue" size:16.0]];
        [postText setTextColor:[UIColor blackColor]];
        //[postText setBackgroundColor:[UIColor yellowColor]];
        [postText setBackgroundColor:[UIColor whiteColor]];
        [postText setDelegate:self];
        [addPost addSubview:postText];
        
        */
        
        
        libraryorcamera = [[UIView alloc] initWithFrame:CGRectMake(50, 180, width-100, 180)];
        libraryorcamera.backgroundColor = [UIColor whiteColor];
        libraryorcamera.layer.cornerRadius = 4;
        libraryorcamera.layer.shadowColor = [UIColor grayColor].CGColor;
        libraryorcamera.layer.shadowRadius = 4;
        libraryorcamera.layer.shadowOpacity = .6;
        libraryorcamera.alpha = 0.0;
        [self.view addSubview:libraryorcamera];
        
        closelibraryorcamera = [[UIButton alloc] initWithFrame:CGRectMake(width-125, 5, 20, 20)];
        closelibraryorcamera.backgroundColor = [UIColor redColor];
        [closelibraryorcamera addTarget:self action:@selector(photolibrarypickerclose) forControlEvents:UIControlEventTouchUpInside];
        closelibraryorcamera.layer.cornerRadius = 10;
        [libraryorcamera addSubview:closelibraryorcamera];
        
        
        UIButton* closeimage = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 10, 10)];
        [closeimage setImage:[UIImage imageNamed:@"closeBtn"] forState:UIControlStateNormal];
        [closelibraryorcamera addSubview:closeimage];
        
        
        UILabel* photoChoose = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, width-100, 40)];
        photoChoose.text = @"Add photo from library or camera?";
        photoChoose.textColor = [UIColor blackColor];
        photoChoose.backgroundColor = [UIColor clearColor];
        [photoChoose setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0]];
        photoChoose.textAlignment = NSTextAlignmentCenter;
        [libraryorcamera addSubview:photoChoose];
        
        camera = [[UIButton alloc] initWithFrame:CGRectMake(0, 45, width-100, 60)];
        camera.backgroundColor = [UIColor clearColor];
        [camera.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:20.0]];
        [camera setTitle:@"Camera" forState:UIControlStateNormal];
        [camera setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [camera.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [camera addTarget:self action:@selector(openPhotoLibrary) forControlEvents:UIControlEventTouchUpInside];
        [libraryorcamera addSubview:camera];
        
        UIView* clline = [[UIView alloc] initWithFrame:CGRectMake(5, 105, width-110, 1)];
        clline.backgroundColor = [UIColor blueColor];
        [libraryorcamera addSubview:clline];
        
        library = [[UIButton alloc] initWithFrame:CGRectMake(0, 105, width-100, 60)];
        library.backgroundColor = [UIColor clearColor];
        [library.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:20.0]];
        [library setTitle:@"Library" forState:UIControlStateNormal];
        [library setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [library.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [library addTarget:self action:@selector(openPhoneLibrary) forControlEvents:UIControlEventTouchUpInside];
        [libraryorcamera addSubview:library];
        
    }
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"event = %@", self.event];
    PFQuery *query = [PFQuery queryWithClassName:@"EventFeed" predicate:pred1];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        
        if (error == nil) {
            [eventFeeds removeAllObjects];
            [eventFeeds addObjectsFromArray:objects];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            //Your main thread code goes in here
            [tableSubPosts reloadData];
        });
    }];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    PFFile *file = [self.event objectForKey:@"picture"];
    if(file.url != nil){
        [avatar sd_setImageWithURL:[NSURL URLWithString:file.url]];
    }
    
    title.text = [self.event objectForKey:@"title"];
    //title.textColor = [UIColor colorWithRed:113/255 green:113/255 blue:113/255 alpha:1.0];
    title.textColor = [UIColor lightGrayColor];
    
    NSString *date1 = [self.event objectForKey:@"date"];
    NSString *from1 = [self.event objectForKey:@"from"];
    
    time.text = [NSString stringWithFormat:@"%@,%@",date1,from1];
    //time.textColor = [UIColor colorWithRed:113/255 green:113/255 blue:113/255 alpha:1.0];
    time.textColor = [UIColor lightGrayColor];
    
    
    location.text = [self.event objectForKey:@"locationname"];
    location.textColor = [UIColor colorWithRed:83.0/255.0 green:152.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    
    description.text = [self.event objectForKey:@"description"];
    //description.textColor = [UIColor colorWithRed:113/255 green:113/255 blue:113/255 alpha:1.0];
    description.textColor = [UIColor lightGrayColor];
    
    url.text = [self.event objectForKey:@"website"];
    url.textColor = [UIColor colorWithRed:81/255 green:81/255 blue:81/255 alpha:1.0];
    
    line.backgroundColor = [UIColor colorWithRed:200.0/255.0 green:93.0/255.0 blue:109.0/255.0 alpha:1.0];
    
    
    PFGeoPoint *checkpoint = [self.event objectForKey:@"location"];
    [map setCenterCoordinate:CLLocationCoordinate2DMake(checkpoint.latitude, checkpoint.longitude)];
    double scalingFactor = ABS( (cos(2 * M_PI * 48.0 / 360.0) ));
    MKCoordinateSpan span;
    span.latitudeDelta = .15/69.0;
    span.longitudeDelta = .15/(scalingFactor * 69.0);
    //[map setRegion:MKCoordinateRegionMake(CLLocationCoordinate2DMake(41.376+.0005, -93.559), span)];
    [map setRegion:MKCoordinateRegionMake(CLLocationCoordinate2DMake(checkpoint.latitude+.0005, checkpoint.longitude), span)];
    
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    //[annotation setCoordinate:CLLocationCoordinate2DMake(41.376, -93.559)];
    [annotation setCoordinate:CLLocationCoordinate2DMake(checkpoint.latitude, checkpoint.longitude)];
    [map addAnnotation:annotation];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    /*
    self.lblEventName.text=[self.event objectForKey:@"title"];
    self.lblLocationName.text=[self.event objectForKey:@"locationname"];
    self.lblDateAndTime.text=[NSString stringWithFormat:@"Begins at %@",[self.event objectForKey:@"from"]];
    self.lblDescription.text=[self.event objectForKey:@"description"];
    self.lblWebsite.text=[self.event objectForKey:@"website"];
    self.imageView.layer.borderWidth=1.0;
    self.imageView.layer.borderColor = [UIColor colorWithRed:19.0/255 green:168.0/255 blue:158.0/255 alpha:1.0].CGColor;
    */
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)goBackVC
{
    [self.navigationController popViewControllerAnimated:YES];
    /*
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    self.view.center = CGPointMake(-(width/2.0), height/2.0);
    [UIView commitAnimations];
    [self scrollReset];
    */
}


-(void)showGridMenu
{
    
    NSArray *items = @[
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"photo-camera"] title:@"Take Photo"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"picture-choose"] title:@"Choose Photo"],
                       [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"editText"] title:@"Text"],
                       ];
    
    RNGridMenu *av = [[RNGridMenu alloc] initWithItems:items];
    av.delegate = self;
    //    av.bounces = NO;
    [av showInViewController:self center:CGPointMake(self.view.bounds.size.width/2.f, self.view.bounds.size.height/2.f)];
}

#pragma mark - RNGridMenuDelegate

- (void)gridMenu:(RNGridMenu *)gridMenu willDismissWithSelectedItem:(RNGridMenuItem *)item atIndex:(NSInteger)itemIndex {
    
    NSLog(@"Dismissed with item %d: %@", itemIndex, item.title);
    
    if(itemIndex==0)
    {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
    
    else if (itemIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    else
    {
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        AddFeedViewController *addFeedViewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"addFeedViewController"];
        addFeedViewController.event = self.event;
        [self.navigationController pushViewController:addFeedViewController animated:YES];
    }
    
}

/*
-(void)moveOutAddPost
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    addPost.center = CGPointMake(width/2.0, (height*2.0));
    [UIView commitAnimations];
    [postText resignFirstResponder];
    [postText setText:@""];
    image = nil;
    [photoButton setImage:nil forState:UIControlStateNormal];
    [photoButton setImage:[UIImage imageNamed:@"placeholderImg"] forState:UIControlStateNormal];
}
*/

-(void)goToAppleMap
{
    PFGeoPoint *checkpoint = [self.event objectForKey:@"location"];
    CLLocationCoordinate2D locationPoint = CLLocationCoordinate2DMake(checkpoint.latitude, checkpoint.longitude);
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:locationPoint
                                                   addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:title.text];
    // Pass the map item to the Maps app
    [mapItem openInMapsWithLaunchOptions:nil];

}

-(void)openPhotoLibrary
{
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.allowsEditing = NO;
    
    [self presentViewController:imagePicker
                       animated:YES completion:nil];
}

-(void)openPhoneLibrary
{
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.allowsEditing = NO;
    
    [self presentViewController:imagePicker
                       animated:YES completion:nil];
}

#pragma mark -
#pragma mark ImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AddFeedViewController *addFeedViewController = [mainStoryBoard instantiateViewControllerWithIdentifier:@"addFeedViewController"];
        addFeedViewController.event = self.event;
        addFeedViewController.image = chosenImage;
        [self.navigationController pushViewController:addFeedViewController animated:YES];
        
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

/*
-(void)createNewPost
{
    if (postText.text.length == 0) {
        return;
    }
    
    
    if ([ESUtility checkIfSameImage:photoButton.imageView.image isEqualTo:[UIImage imageNamed:@"placeholderImg"]]) {
        return;
    }
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"h:mma"];
    NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
    
    PFObject *eventFeed = [PFObject objectWithClassName:@"EventFeed"];
    [eventFeed setObject:[PFUser currentUser] forKey:@"user"];
    [eventFeed setObject:self.event forKey:@"event"];
    [eventFeed setObject:description.text?:@"" forKey:@"comment"];
    [eventFeed setObject:dateString forKey:@"time"];
    if(photoButton.imageView.image != nil)
    {
        PFFile *picture = [PFFile fileWithData:UIImageJPEGRepresentation(photoButton.imageView.image, 0.5)];
        [eventFeed setObject:picture forKey:@"picture"];
    }
    
    
    [eventFeed saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
    
        NSLog(@"event comments added");
        
    }];
    
    [self moveOutAddPost];
}
*/


-(UIImage*)scaleImage:(UIImage*)selectedImage ratio:(float)ratio
{
    CGImageRef simage = selectedImage.CGImage;
    
    float pwidth = CGImageGetWidth(simage)/ratio;
    float pheight = CGImageGetHeight(simage)/ratio;
    float bitsPerComponent = CGImageGetBitsPerComponent(simage);
    float bytesPerRow = CGImageGetBytesPerRow(simage);
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(simage);
    float bitmapInfo = CGImageGetBitmapInfo(simage);
    
    CGContextRef context = CGBitmapContextCreate(nil, pwidth, pheight, bitsPerComponent, bytesPerRow, colorSpace, bitmapInfo);
    
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    
    CGContextDrawImage(context, CGRectMake(0, 0, pwidth, pheight), simage);
    
    UIImage* scaledImage = [[UIImage alloc]initWithCGImage:CGBitmapContextCreateImage(context)];
    
    return scaledImage;
}

- (UIImage *)fixOrientation:(UIImage*)newImage {
    
    // No-op if the orientation is already correct
    if (newImage.imageOrientation == UIImageOrientationUp) return newImage;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (newImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, newImage.size.width, newImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, newImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, newImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (newImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, newImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, newImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, newImage.size.width, newImage.size.height,
                                             CGImageGetBitsPerComponent(newImage.CGImage), 0,
                                             CGImageGetColorSpace(newImage.CGImage),
                                             CGImageGetBitmapInfo(newImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (newImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,newImage.size.height,newImage.size.width), newImage.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,newImage.size.width,newImage.size.height), newImage.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}


-(void)photolibrarypicker
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.4];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    libraryorcamera.alpha = 1.0;
    [UIView commitAnimations];
    [postText resignFirstResponder];
}



-(void)photolibrarypickerclose
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.4];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    libraryorcamera.alpha = 0.0;
    [UIView commitAnimations];
}

#pragma mark -
#pragma mark TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [eventFeeds count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *eventFeed = [eventFeeds objectAtIndex:[eventFeeds count]-indexPath.row-1];
    UITableViewCell *cell ;
    PFUser *user = [eventFeed objectForKey:@"user"];
    [user fetchIfNeeded];
    if([eventFeed objectForKey:@"picture"])
    {
        cell =[tableView dequeueReusableCellWithIdentifier:@"cell2"];
        UILabel *username = [cell.contentView viewWithTag:1];
        UILabel *time = [cell.contentView viewWithTag:2];
        UILabel *comment = [cell.contentView viewWithTag:3];
        UIImageView *imageView = [cell.contentView viewWithTag:4];
        if(user.isDataAvailable)
            username.text = [user objectForKey:@"displayName"];
        else
            username.text = @"";
        
        time.text = [eventFeed objectForKey:@"time"];
        comment.text = [eventFeed objectForKey:@"comment"];
        PFFile *file = [eventFeed objectForKey:@"picture"];
        if(file.url!=nil)
            [imageView sd_setImageWithURL:[NSURL URLWithString:file.url]];
        
        //        FIRStorageReference *storageRef = [[[FIRStorage storage] reference] child:[eventFeed objectForKey:@"photopath"]];
        //        [storageRef dataWithMaxSize:1 * 512 * 512 completion:^(NSData * _Nullable data, NSError * _Nullable error) {
        //            UIImage *image = [UIImage imageWithData:data];
        //            imageView.image = image;
        //
        //        }];
        
    }
    else
    {
        cell =[tableView dequeueReusableCellWithIdentifier:@"cell1"];
        
        UILabel *username = [cell.contentView viewWithTag:1];
        UILabel *time = [cell.contentView viewWithTag:2];
        UILabel *comment = [cell.contentView viewWithTag:3];
        if(user.isDataAvailable)
            username.text = [user objectForKey:@"displayName"];
        else
            username.text = @"";
        
        time.text = [eventFeed objectForKey:@"time"];
        comment.text = [eventFeed objectForKey:@"comment"];
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *eventFeed = [eventFeeds objectAtIndex:[eventFeeds count]-indexPath.row-1];
    CGSize constraint = CGSizeMake([[UIApplication sharedApplication] keyWindow].frame.size.width-140, CGFLOAT_MAX);
    CGSize size;
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [[eventFeed objectForKey:@"comment"]  boundingRectWithSize:constraint
                                                                            options:NSStringDrawingUsesLineFragmentOrigin
                                                                         attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Avenir Medium" size:13.0]}
                                                                            context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    if([eventFeed objectForKey:@"picture"]!=nil)
        return MAX(150.0, size.height+125);
    else
        return MAX(55.0, size.height+10);
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
