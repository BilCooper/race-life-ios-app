//
//  AddFeedViewController.h
//  Do-Invision
//
//  Created by Rupesh on 7/2/17.
//  Copyright © 2017 Mark Feaver. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddFeedViewController : UIViewController
@property(nonatomic,strong)NSDictionary *event;
@property(nonatomic,assign)BOOL isImage;
@property(nonatomic,strong)UIImage *image;

@end
