//
//  CalendarViewController.h
//  Do-Invision
//
//  Created by Rupesh on 4/15/17.
//  Copyright © 2017 Mark Feaver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JTCalendar.h"
@import CoreLocation;
@interface CalendarViewController : UIViewController<JTCalendarDelegate,CLLocationManagerDelegate>

@end
