//
//  AddNewEventViewController.m
//  Do-Invision
//
//  Created by Rupesh on 4/19/17.
//  Copyright © 2017 Mark Feaver. All rights reserved.
//

#import "AddNewEventViewController.h"
#import "SearchLocationViewController.h"

//@import FirebaseAuth;
//@import FirebaseDatabase;
//@import FirebaseStorage;

@interface AddNewEventViewController ()
@property(nonatomic,weak)IBOutlet UITextView *txtTitle;
@property(nonatomic,weak)IBOutlet UITextView *txtDescription;
@property(nonatomic,weak)IBOutlet UITextField *txtDate;
@property(nonatomic,weak)IBOutlet UITextField *txtFrom;
@property(nonatomic,weak)IBOutlet UITextField *txtLocation;
@property(nonatomic,weak)IBOutlet UITextField *txtUrl;
@property(nonatomic,weak)IBOutlet UIScrollView *scroll;
@property (weak,nonatomic)IBOutlet UIButton *imageBtn;
@property (weak,nonatomic)IBOutlet UIBarButtonItem *doneBtn;

@property(nonatomic,strong)UIDatePicker *datePicker;
@property(nonatomic,strong)NSDateFormatter *dateFormat;
@property(nonatomic,strong)UITextField *selectedTextField;
@property(nonatomic,strong)NSData *imageData;
@property(nonatomic,strong)UIToolbar *toolBar;
@property(nonatomic,assign)CGFloat latitude;
@property(nonatomic,assign)CGFloat longitude;

@end

@implementation AddNewEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.datePicker = [[UIDatePicker alloc]init];
    [self.datePicker setDate:[NSDate date]];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.dateFormat = [[NSDateFormatter alloc] init];
    [self.dateFormat setAMSymbol:@"am"];
    [self.dateFormat setPMSymbol:@"pm"];
    self.txtTitle.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.txtDescription.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.doneBtn.enabled = NO;
    
    _toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [_toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePicker)];
    UIBarButtonItem *cancelBtn=[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPicker)];
    
    UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [_toolBar setItems:[NSArray arrayWithObjects:cancelBtn,space,doneBtn, nil]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
    //[self inputDummyData];
}

-(void)inputDummyData{
    self.txtTitle.text = @"Some race 2";
    self.txtDescription.text = @"Description goes here, and all the description should be good enought to understand by the team";
    self.txtUrl.text = @"www.dailymotion.com";
}

-(void) donePicker{
    
    NSDate *eventDate = self.datePicker.date;
    if(self.selectedTextField == self.txtDate)
    {
        [self.dateFormat setDateFormat:@"MMMM dd, yyyy"];
        NSString *dateString = [self.dateFormat stringFromDate:eventDate];
        self.txtDate.text = [NSString stringWithFormat:@"%@",dateString];
        [self.txtDate resignFirstResponder];

    }
    else if(self.selectedTextField == self.txtFrom)
    {
        [self.dateFormat setDateFormat:@"h:mma"];
        NSString *dateString = [self.dateFormat stringFromDate:eventDate];
        self.txtFrom.text = [NSString stringWithFormat:@"%@",dateString];
        [self.txtFrom resignFirstResponder];
    }

}
-(void)cancelPicker
{
    [self.txtDate resignFirstResponder];
    [self.txtFrom resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    [self checkValidation];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string   // return NO to not change text
{
    [self checkValidation];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField==self.txtFrom)
    {
        
        [self.txtFrom setInputAccessoryView:_toolBar];
        [self.txtFrom setInputView:self.datePicker];
        [self.datePicker setDatePickerMode:UIDatePickerModeTime];

    }
    else if(textField==self.txtDate)

  {
      [self.txtDate setInputAccessoryView:_toolBar];
        [self.txtDate setInputView:self.datePicker];
      [self.datePicker setDatePickerMode:UIDatePickerModeDate];

    }
//    else if (textField == self.txtLocation)
//    {
//        [self.scroll setContentOffset:CGPointMake(0, self.scroll.contentSize.height-textField.frame.origin.y) animated:YES];
//    }
//    else if (textField == self.txtUrl)
//    {
//        [self.scroll setContentOffset:CGPointMake(0, self.scroll.contentSize.height-textField.frame.origin.y) animated:YES];
//    }
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.selectedTextField = textField;

    if(textField==self.txtLocation)
    {
        SearchLocationViewController *searchLocationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"searchLocationViewController"];
        searchLocationViewController.delegate=self;
        [self.navigationController presentViewController:searchLocationViewController animated:YES completion:^{
            
        }];

        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(IBAction)done:(id)sender
{
    if(self.txtTitle.text.length==0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter title" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:^{
        }];

    }
    else if(self.txtDate.text.length==0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter date" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:^{
        }];

    }
    else if(self.txtFrom.text.length==0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter start time" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:^{
        }];

    }
    else if(self.txtLocation.text.length==0)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter location" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:^{
        }];

    }
    else
    {
        PFObject *event = [PFObject objectWithClassName:@"Event"];
        [event setObject:[PFUser currentUser] forKey:@"user"];
        [event setObject:self.txtTitle.text forKey:@"title"];
        [event setObject:self.txtDescription.text?:@"" forKey:@"description"];
        [event setObject:self.txtDate.text forKey:@"date"];
        [event setObject:self.txtFrom.text forKey:@"from"];
        [event setObject:@"11:59 PM" forKey:@"to"];
        [event setObject:self.txtLocation.text forKey:@"locationname"];
        PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:self.latitude longitude:self.longitude];
        [event setObject:geoPoint forKey:@"location"];
        [event setObject:self.txtUrl.text?:@"" forKey:@"website"];
        if(self.imageData!=nil)
        {
          PFFile *picture = [PFFile fileWithData:self.imageData];
          [event setObject:picture forKey:@"picture"];
        }
        [event saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            
            if (succeeded) {
                NSLog(@"Event saved successfully");
            }
            
        }];
    
        /*
        FIRUser *user = [FIRAuth auth].currentUser;
        FIRDatabaseReference *databaseRef = [[FIRDatabase database] reference];
        
        NSString *photoPath = @"";
        if(self.imageData!=nil)
        {
            photoPath = [NSString stringWithFormat:@"images/%@/%@.jpg",user.uid,[NSDate date]];
            FIRStorageReference *storageRef = [[[FIRStorage storage] reference] child:photoPath];
            
            [storageRef putData:self.imageData
                       metadata:nil
                     completion:^(FIRStorageMetadata *metadata,
                                  NSError *error) {
                         if (error != nil) {
                             // Uh-oh, an error occurred!
                         } else {
                             // Metadata contains file metadata such as size, content-type, and download URL.
                             // NSURL *downloadURL = metadata.downloadURL;
                         }
                     }];
        }
        [[[[databaseRef child:@"events"] child:user.uid] childByAutoId]
         setValue:@{@"title": self.txtTitle.text,
                    @"description": self.txtDescription.text?:@"",
                    @"date": self.txtDate.text,@"from": self.txtFrom.text,
                    @"to":@"11:59 PM",@"location": self.txtLocation.text,
                    @"website": self.txtUrl.text?:@"",
                    @"photopath":photoPath,
                    @"latitude":[NSNumber numberWithDouble:self.latitude],
                    @"longitude":[NSNumber numberWithDouble:self.longitude]}];
        */
        
        
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(IBAction)cancel:(id)sender
{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure you want to discard your new event?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
        
    }];
    [alert addAction:noAction];

    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }];

    [alert addAction:yesAction];
    [self presentViewController:alert animated:YES completion:^{
    }];


}

- (IBAction)selectPhoto:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"Please select photo from" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }]];
    actionSheet.popoverPresentationController.sourceView = self.view;
    actionSheet.popoverPresentationController.sourceRect =self.imageBtn.frame;
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    UIImage *resizedImage = [self imageWithImage:chosenImage scaledToSize:CGSizeMake(512, 512)];
    [self.imageBtn setBackgroundImage:resizedImage forState:UIControlStateNormal];
    self.imageData=UIImageJPEGRepresentation(resizedImage, 0.5);
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)keyboardWasShown:(NSNotification *)aNotification

{// scroll to the text view
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height+70, 0.0);
    self.scroll.contentInset = contentInsets;
    self.scroll.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible.
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.selectedTextField.frame.origin) ) {
        [self.scroll scrollRectToVisible:self.selectedTextField.frame animated:YES];
    }

}
-(void)checkValidation
{
    if(self.txtTitle.text.length!=0&&self.txtDate.text.length!=0&&self.txtFrom.text.length!=0&&self.txtLocation.text!=0)
        self.doneBtn.enabled = YES;
    else
        self.doneBtn.enabled = NO;

}
- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    // scroll back..
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scroll.contentInset = contentInsets;
    self.scroll.scrollIndicatorInsets = contentInsets;
}

-(void)locationSelected:(NSString*)location withLatitude:(CGFloat)latitude andLongitude:(CGFloat)longitude
{
    [self checkValidation];
    self.txtLocation.text = location;
    self.latitude = latitude;
    self.longitude = longitude;
    
}

- (void) dismissKeyboard
{
    // add self
    
    [self.selectedTextField resignFirstResponder];
    [self.txtTitle resignFirstResponder];
    [self.txtDescription resignFirstResponder];

    
}


- (void)viewWillDisappear:(BOOL)animated
{
    // unregister for keyboard notifications while not visible.
  //  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
  //  [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
