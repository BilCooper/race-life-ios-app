//
//  HelperParse.m
//  SocialNetwork
//
//  Created by Yasir on 1/14/18.
//  Copyright © 2018 Sugarsage. All rights reserved.
//

#import "HelperParse.h"

@implementation HelperParse


+ (instancetype)sharedInstance
{
    static HelperParse *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[HelperParse alloc] init];
        
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}


-(void)saveParseObjectWithPara:(NSDictionary *)param withHandler:(void(^)(NSString *status))handler{
    
    PFObject *event = [PFObject objectWithClassName:@"Event"];
    [event setObject:[PFUser currentUser] forKey:@"user"];
    [event setObject:[param objectForKey:@"titleIn"] forKey:@"title"];
    [event setObject:[param objectForKey:@"descriptionIn"] forKey:@"description"];
    [event setObject:[param objectForKey:@"dateIn"] forKey:@"date"];
    
    [event setObject:[param objectForKey:@"moreIn"] forKey:@"more"];
    
    
    
    //[event setObject:self.txtFrom.text forKey:@"from"];
    //[event setObject:@"11:59 PM" forKey:@"to"];
    
    //[event setObject:@"4:00 Pm" forKey:@"from"];
    
    [event setObject:[param objectForKey:@"fromIn"] forKey:@"from"];
    //[event setObject:@"11:59 PM" forKey:@"to"];
    
    [event setObject:[param objectForKey:@"locationNameIn"] forKey:@"locationname"];
    
    CGFloat someLatitude = [[param objectForKey:@"latitudeIn"] floatValue];
    CGFloat someLongitude = [[param objectForKey:@"longitudeIn"] floatValue];
    PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:someLatitude longitude:someLongitude];
    
    [event setObject:geoPoint forKey:@"location"];
    
    //moreIn
    [event setObject:[param objectForKey:@"moreIn"] forKey:@"website"];
    
    //cell.imgvQuotes.image = [UIImage imageNamed:[productDictionary objectForKey:@"image_path"]];
    
    if([param objectForKey:@"imageDataIn"])
    {
        PFFile *picture = [PFFile fileWithData:[param objectForKey:@"imageDataIn"]];
        [event setObject:picture forKey:@"picture"];
    }
    [event saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        
        if (succeeded) {
            NSLog(@"Event saved successfully");
            
            handler(@"Success");
        }
        
    }];
    
    
}

- (BOOL)image:(UIImage *)image1 isEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    
    return [data1 isEqual:data2];
}

@end
