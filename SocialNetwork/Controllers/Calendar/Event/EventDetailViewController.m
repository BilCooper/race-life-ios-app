//
//  EventDetailViewController.m
//  eventmanagement
//
//  Created by Nick Schulze on 2/21/15.
//  Copyright (c) 2015 infusion infotech. All rights reserved.
//

#import "EventDetailViewController.h"

@implementation EventDetailViewController

//@synthesize locationManager = _locationManager;
//@synthesize delegate = _delegate;
@synthesize /*parse,*/ delegate;

enum {
    ChoosingStartDate = 5,
    ChoosingEndDate = 6,
    ChoosingNoDate = 7
};

-(id)init
{
    if(self)
    {
        UIScreen* main = [UIScreen mainScreen];
        width = main.bounds.size.width;
        height = main.bounds.size.height;
        
        self.view.frame = CGRectMake(-width, 0, width, height);
        self.view.backgroundColor = [UIColor whiteColor];
        
        background = [[UIScrollView  alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        background.backgroundColor = [UIColor clearColor];
        background.contentSize = CGSizeMake(width, height+136);
        [background setShowsVerticalScrollIndicator:NO];
        background.delegate = self;
        background.scrollEnabled = NO;
        background.tag = 1;
        [self.view addSubview:background];
        
        UIView* borderRight = [[UIView alloc] initWithFrame:CGRectMake(width, 0, 1, height)];
        borderRight.backgroundColor = [UIColor black];
        [background addSubview:borderRight];
        
        map = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, width, 88)];
        [map setMapType:MKMapTypeStandard];
        [map setCenterCoordinate:CLLocationCoordinate2DMake(48, -93)];
        double scalingFactor = ABS( (cos(2 * M_PI * 48.0 / 360.0) ));
        MKCoordinateSpan span;
        span.latitudeDelta = .15/69.0;
        span.longitudeDelta = .15/(scalingFactor * 69.0);
        [map setRegion:MKCoordinateRegionMake(CLLocationCoordinate2DMake(41.376+.0005, -93.559), span)];
        [map setUserInteractionEnabled:NO];
        [map setDelegate:self];
        [self.view addSubview:map];
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        //PFGeoPoint* checkpoint = object[@"Coordinates"];
        [annotation setCoordinate:CLLocationCoordinate2DMake(41.376, -93.559)];
        [map addAnnotation:annotation];
        
        back = [[UIButton alloc] initWithFrame:CGRectMake(0, 4, 64, 64)];
        back.backgroundColor = [UIColor clearColor];
        [back addTarget:self action:@selector(moveOut) forControlEvents:UIControlEventTouchUpInside];
        [back setImage:[UIImage imageNamed:@"left carrot.png"] forState:UIControlStateNormal];
        [self.view addSubview:back];
        
        post = [[UIButton alloc] initWithFrame:CGRectMake(width-64, 11, 64, 64)];
        post.backgroundColor = [UIColor clearColor];
        [post setImage:[UIImage imageNamed:@"post.png"] forState:UIControlStateNormal];
        [post addTarget:self action:@selector(moveInAddPost) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:post];
        
        avatar = [[UIImageView alloc] initWithFrame:CGRectMake(10, 68, 88, 88)];
        avatar.backgroundColor = [UIColor whiteColor];
        avatar.layer.cornerRadius = 44;
        avatar.clipsToBounds = YES;
        avatar.contentMode = UIViewContentModeScaleAspectFill;
        [self.view addSubview:avatar];
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(108, 88, width-118, 60)];
        title.textColor = [UIColor black];
        title.backgroundColor = [UIColor clearColor];
        title.numberOfLines = 2;
        [title setFont:[UIFont fontWithName:@"Avenir" size:20.0]];
        [background addSubview:title];
        
        headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 28, width, 60)];
        headerView.backgroundColor = [UIColor whiteColor];
        headerView.alpha = 0.0;
        [map addSubview:headerView];
        
        headerTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, 46, width-40, 40)];
        headerTitle.textColor = [UIColor red];
        headerTitle.backgroundColor = [UIColor clearColor];
        headerTitle.textAlignment = NSTextAlignmentCenter;
        [headerTitle setFont:[UIFont fontWithName:@"Avenir" size:18.0]];
        headerTitle.alpha = 0.0;
        [map addSubview:headerTitle];
        
        time = [[UILabel alloc] initWithFrame:CGRectMake(20, 160, width-20, 30)];
        time.text = @"";
        time.textColor = [UIColor black];
        time.backgroundColor = [UIColor clearColor];
        [time setFont:[UIFont fontWithName:@"Avenir" size:13.0]];
        [background addSubview:time];
        
        locationPointer = [[UIImageView alloc] initWithFrame:CGRectMake(15, 185, 20, 20)];
        locationPointer.backgroundColor = [UIColor clearColor];
        [locationPointer setImage:[UIImage imageNamed:@"vuu icon.png"]];
        [background addSubview:locationPointer];
        
        location = [[UILabel alloc] initWithFrame:CGRectMake(35, 185, width-40, 20)];
        location.text = @"";
        [location setTextAlignment:NSTextAlignmentLeft];
        location.textColor = [UIColor lightBlue];
        location.backgroundColor = [UIColor clearColor];
        [location setFont:[UIFont fontWithName:@"Avenir-Light" size:13.0]];
        [background addSubview:location];
        
        locationButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 185, width-20, 30)];
        locationButton.backgroundColor = [UIColor clearColor];
        [locationButton addTarget:self action:@selector(openInMap) forControlEvents:UIControlEventTouchUpInside];
        [background addSubview:locationButton];
        
        description = [[UITextView alloc] initWithFrame:CGRectMake(15, 208, width-20, 64)];
        [description setUserInteractionEnabled:NO];
        description.text = @"";
        description.textColor = [UIColor black];
        description.backgroundColor = [UIColor clearColor];
        [description setFont:[UIFont fontWithName:@"Avenir-Light" size:13.0]];
        [background addSubview:description];
        
        address = [[UILabel alloc] initWithFrame:CGRectMake(10, 160, 120, 40)];
        address.text = @"";
        address.backgroundColor = [UIColor redColor];
        //[self.view addSubview:title];
        
        url = [[UITextView alloc] initWithFrame:CGRectMake(15, 265, (width-30), 40)];
        url.text = @"";
        [url setTextAlignment:NSTextAlignmentLeft];
        url.textColor = [UIColor black];
        url.tintColor = [UIColor lightBlue];
        url.backgroundColor = [UIColor clearColor];
        url.clipsToBounds =YES;
        [url setFont:[UIFont fontWithName:@"Avenir-Light" size:11.0]];
        url.editable = NO;
        url.dataDetectorTypes = UIDataDetectorTypeLink;
        [background addSubview:url];
        
        letter = [[UILabel alloc]initWithFrame:CGRectMake(3, 6, 80, 78)];
        letter.textColor = [UIColor lightBlue];
        [letter setTextAlignment:NSTextAlignmentCenter];
        [letter setFont:[UIFont fontWithName:@"Avenir-Light" size:60.0]];
        [avatar addSubview:letter];
        
        UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, 299, width, 1)];
        line.backgroundColor = [UIColor red];
        [background addSubview:line];
        
        posts = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 300, width, height-60)];
        posts.contentSize = CGSizeMake(width, height-60);
        [posts setBackgroundColor:[UIColor whiteColor]];
        posts.delegate = self;
        posts.tag = 2;
        [background addSubview:posts];
        
        addPost = [[UIView alloc] initWithFrame:CGRectMake(0, height+224, width, height)];
        addPost.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:addPost];
        
        postLine = [[UIView alloc] initWithFrame:CGRectMake(0, 64, width, 1)];
        postLine.backgroundColor = [UIColor red];
        [addPost addSubview:postLine];
        
        postText = [[UITextView alloc] initWithFrame:CGRectMake(10, 74, width-20, 190)];
        [postText setFont:[UIFont fontWithName:@"Avenir-Light" size:16.0]];
        [postText setTextColor:[UIColor black]];
        [postText setDelegate:self];
        postText.delegate = self;
        [addPost addSubview:postText];
        
        cancel = [[UIButton alloc] initWithFrame:CGRectMake(10, 20, 80, 44)];
        cancel.backgroundColor = [UIColor clearColor];
        [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
        cancel.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [cancel.titleLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:18.0]];
        [cancel setTitleColor:[UIColor red] forState:UIControlStateNormal];
        [cancel addTarget:self action:@selector(moveOutAddPost) forControlEvents:UIControlEventTouchUpInside];
        [addPost addSubview:cancel];
        
        postTitle = [[UILabel alloc] initWithFrame:CGRectMake((width/2)-40, 20, 80, 44)];
        [postTitle setFont:[UIFont fontWithName:@"Avenir" size:18.0]];
        [postTitle setTextColor:[UIColor black]];
        [postTitle setText:@"New Post"];
        [addPost addSubview:postTitle];
        
        makePost = [[UIButton alloc] initWithFrame:CGRectMake(width-90, 20, 80, 44)];
        makePost.backgroundColor = [UIColor clearColor];
        [makePost setTitle:@"Add" forState:UIControlStateNormal];
        makePost.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [makePost.titleLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:18.0]];
        [makePost setTitleColor:[UIColor red] forState:UIControlStateNormal];
        [makePost addTarget:self action:@selector(newPost) forControlEvents:UIControlEventTouchUpInside];
        [addPost addSubview:makePost];
        
        photoButton = [[UIButton alloc] initWithFrame:CGRectMake(10, height-319, 60, 60)];
        photoButton.backgroundColor = [UIColor lightBlue];
        photoButton.clipsToBounds = YES;
        [photoButton addTarget:self action:@selector(photolibrarypicker) forControlEvents:UIControlEventTouchUpInside];
        [addPost addSubview:photoButton];
        
        photo = [[UIImageView alloc] initWithFrame:CGRectMake(20, height-306, 40, 30)];
        [photo setImage:[UIImage imageNamed:@"camera.png"]];
        [photo setClipsToBounds:YES];
        [addPost addSubview:photo];
        
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        
        libraryorcamera = [[UIView alloc] initWithFrame:CGRectMake(50, 180, width-100, 180)];
        libraryorcamera.backgroundColor = [UIColor whiteColor];
        libraryorcamera.layer.cornerRadius = 4;
        libraryorcamera.layer.shadowColor = [UIColor gray].CGColor;
        libraryorcamera.layer.shadowRadius = 4;
        libraryorcamera.layer.shadowOpacity = .6;
        libraryorcamera.alpha = 0.0;
        [self.view addSubview:libraryorcamera];
        
        closelibraryorcamera = [[UIButton alloc] initWithFrame:CGRectMake(width-125, 5, 20, 20)];
        closelibraryorcamera.backgroundColor = [UIColor red];
        [closelibraryorcamera addTarget:self action:@selector(photolibrarypickerclose) forControlEvents:UIControlEventTouchUpInside];
        closelibraryorcamera.layer.cornerRadius = 10;
        [libraryorcamera addSubview:closelibraryorcamera];
        
        UIButton* closeimage = [[UIButton alloc] initWithFrame:CGRectMake(5, 5, 10, 10)];
        [closeimage setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
        [closelibraryorcamera addSubview:closeimage];
        
        UILabel* photoChoose = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, width-100, 40)];
        photoChoose.text = @"Add photo from library or camera?";
        photoChoose.textColor = [UIColor blackColor];
        photoChoose.backgroundColor = [UIColor clearColor];
        [photoChoose setFont:[UIFont fontWithName:@"Avenir-Light" size:12.0]];
        photoChoose.textAlignment = NSTextAlignmentCenter;
        [libraryorcamera addSubview:photoChoose];
        
        camera = [[UIButton alloc] initWithFrame:CGRectMake(0, 45, width-100, 60)];
        camera.backgroundColor = [UIColor clearColor];
        [camera.titleLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:20.0]];
        [camera setTitle:@"Camera" forState:UIControlStateNormal];
        [camera setTitleColor:[UIColor lightBlue] forState:UIControlStateNormal];
        [camera.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [camera addTarget:self action:@selector(photo) forControlEvents:UIControlEventTouchUpInside];
        [libraryorcamera addSubview:camera];
        
        UIView* clline = [[UIView alloc] initWithFrame:CGRectMake(5, 105, width-110, 1)];
        clline.backgroundColor = [UIColor lightBlue];
        [libraryorcamera addSubview:clline];
        
        library = [[UIButton alloc] initWithFrame:CGRectMake(0, 105, width-100, 60)];
        library.backgroundColor = [UIColor clearColor];
        [library.titleLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:20.0]];
        [library setTitle:@"Library" forState:UIControlStateNormal];
        [library setTitleColor:[UIColor lightBlue] forState:UIControlStateNormal];
        [library.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [library addTarget:self action:@selector(library) forControlEvents:UIControlEventTouchUpInside];
        [libraryorcamera addSubview:library];
        
        comments = [[EventDetailCommentViewController alloc] init];
        [self.view addSubview:comments.view];
        
        [self.view bringSubviewToFront:libraryorcamera];
        shift = 0;
        
        refreshing = NO;
        
        pullRefresh = [[UILabel alloc] initWithFrame:CGRectMake(width/2-100, -35, 200, 30)];
        pullRefresh.text = @"Pull To Refresh";
        [pullRefresh setTextColor:[UIColor lightBlue]];
        [pullRefresh setTextAlignment:NSTextAlignmentCenter];
        [pullRefresh setFont:[UIFont fontWithName:@"Avenir-Light" size:18.0]];
        pullRefresh.alpha = 0.0;
        [posts addSubview:pullRefresh];
        
        imageBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        imageBackground.backgroundColor = [UIColor black];
        imageBackground.alpha = 0;
        imageBackground.hidden = YES;
        [self.view addSubview:imageBackground];
        
        popoutImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, (height/2)-(width/2), width, width)];
        popoutImage.alpha = 0;
        popoutImage.hidden = YES;
        [self.view addSubview:popoutImage];
        
        closePopout = [[UIButton alloc] initWithFrame:CGRectMake(width-40, 20, 30, 30)];
        closePopout.alpha = 0;
        closePopout.hidden = YES;
        [closePopout addTarget:self action:@selector(popoutDismiss) forControlEvents:UIControlEventTouchUpInside];
        closePopout.layer.cornerRadius = 10;
        [self.view addSubview:closePopout];
        
        UIImageView* closePopoutImg = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 15, 15)];
        [closePopoutImg setImage:[UIImage imageNamed:@"close.png"]];
        [closePopout addSubview:closePopoutImg];
        refreshing = NO;
        
        editButton = [[UIButton alloc] initWithFrame:CGRectMake(width-85, 24, 40, 26)];
        [editButton setTitle:@"Edit" forState:UIControlStateNormal];
        editButton.backgroundColor = [UIColor red];
        editButton.layer.cornerRadius = 13;
        [editButton.titleLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:12.0]];
        [editButton addTarget:self action:@selector(editEvent) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:editButton];
        
        items = 0;
    }
    return self;
}

-(void)openInMap
{
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:locationPoint
                                                   addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:title.text];
    // Pass the map item to the Maps app
    [mapItem openInMapsWithLaunchOptions:nil];
}

-(void)setDetail:(PFObject*)_object
{
    object = _object;
    title.text = object[@"Title"];
    [self resizeTitle];
    headerTitle.text = object[@"Title"];
    location.text = object[@"LocationName"];
    description.text = object[@"Description"];
    [url removeFromSuperview];
    url = [[UITextView alloc] initWithFrame:CGRectMake(15, 265, (width-30), 40)];
    url.text = object[@"Url"];
    [url setTextAlignment:NSTextAlignmentLeft];
    url.textColor = [UIColor black];
    url.backgroundColor = [UIColor clearColor];
    [url setFont:[UIFont fontWithName:@"Avenir-Light" size:11.0]];
    url.editable = NO;
    url.dataDetectorTypes = UIDataDetectorTypeLink;
    [background addSubview:url];
    BOOL isAllDay = [object[@"AllDay"] boolValue];
    if(isAllDay)
    {
        time.text = [NSString stringWithFormat:@"%@, All day", object[@"Day"]];
    }
    else
    {
        time.text = [NSString stringWithFormat:@"%@, %@-%@", object[@"Day"], object[@"StartTime"], object[@"EndTime"]];
    }
    
    
    if([object[@"hasImage"] boolValue])
    {
        PFFile* file = object[@"Image"];
        [file getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
            if (!error) {
                UIImage *img = [UIImage imageWithData:data];
                [avatar setImage:img];
            }
        }];
        letter.hidden = YES;
    }
    else
    {
        letter.text = [title.text substringToIndex:1];
        letter.hidden = NO;
        avatar.image = nil;
    }
    
    PFGeoPoint* point = object[@"Coordinates"];
    if(point != nil)
    {
        locationPoint = CLLocationCoordinate2DMake(point.latitude, point.longitude);
        [map setCenterCoordinate:locationPoint];
        double scalingFactor = ABS( (cos(2 * M_PI * 48.0 / 360.0) ));
        MKCoordinateSpan span;
        span.latitudeDelta = .15/69.0;
        span.longitudeDelta = .15/(scalingFactor * 69.0);
        [map setRegion:MKCoordinateRegionMake(CLLocationCoordinate2DMake(point.latitude+.0005, point.longitude), span)];
        [map setUserInteractionEnabled:NO];
        [map setDelegate:self];
        
        UIVisualEffect *blurEffect;
        blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        
        UIVisualEffectView *visualEffectView;
        visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        visualEffectView.frame = map.bounds;
        //[map addSubview:visualEffectView];
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        //PFGeoPoint* checkpoint = object[@"Coordinates"];
        [annotation setCoordinate:locationPoint];
        [map addAnnotation:annotation];
        locationPointer.hidden = NO;
    }
    else
    {
        locationPointer.hidden = YES;
    }
    
    BOOL privacy = [object[@"Privacy"] boolValue];
    if (privacy)
    {
        locationPointer.hidden = YES;
        location.hidden = YES;
    }
    else
    {
        locationPointer.hidden = NO;
        location.hidden = NO;
    }
    [self resetPosts];
    
    PFUser *user = [PFUser currentUser];
    NSString* objectId = [NSString stringWithFormat:@"%@", object[@"UserId"]];
    if ([objectId isEqualToString:user.objectId])
    {
        editButton.hidden = NO;
    }
    else
    {
        editButton.hidden = YES;
    }
}

-(void)editEvent
{
    [self moveOut];
    [[self delegate] editEvent:object];
}

-(void)resizeTitle
{
    float fsize = 20;
    [title setFont:[UIFont fontWithName:@"Avenir" size:fsize]];
    CGSize size = [title.text sizeWithAttributes:@{NSFontAttributeName : title.font}];
    while (size.width > title.bounds.size.width)
    {
        [title setFont:[UIFont fontWithName:@"Avenir" size:fsize]];
        size = [title.text sizeWithAttributes:@{NSFontAttributeName : title.font}];
        fsize -= .50;
    }
}

-(void)setPosts:(NSArray*)postArray
{
    [self resetPosts];
    [posts addSubview:pullRefresh];
    for (int i = 0; i < [postArray count]; i++)
    {
        items += 1;
        PFObject* obj = [postArray objectAtIndex:i];
        
        EventDetailView* detail = [[EventDetailView alloc] init:obj withShift:shift andLine:NO];
        [detail setParse:parse];
        detail.delegate = self;
        [posts addSubview:detail];
        
        shift += [detail getShift];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        refreshing = NO;
    });
    posts.contentSize = CGSizeMake(width, height+shift);
}

-(void)addPosts:(NSArray*)postArray
{
    for (int i = 0; i < [postArray count]; i++)
    {
        items += 1;
        PFObject* obj = [postArray objectAtIndex:i];
        
        EventDetailView* detail = [[EventDetailView alloc] init:obj withShift:shift andLine:NO];
        detail.delegate = self;
        [posts addSubview:detail];
        
        shift += [detail getShift];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        refreshing = NO;
    });
    posts.contentSize = CGSizeMake(width, height+shift);
}

-(void)setComment:(PFObject*)_object
{
    [comments setParse:parse];
    [self.view bringSubviewToFront:comments.view];
    [comments moveIn];
    [comments setComment:_object];
}

-(void)setComments:(NSArray*)postArray
{
    [comments setComments:postArray];
}

-(void)newPost
{
    if([postText.text isEqualToString:@""] == TRUE && image == nil)
    {
        [AppUtils popupAlert:@"" Message:@"You can't make a post without a photo or text."];
        return;
    }
    
    if([userPoint distanceInKilometersTo:object[@"Coordinates"]] > 3)
    {
        [AppUtils popupAlert:@"" Message:@"You can't post to this event!\n You can only post to events you are attending!"];
        return;
    }
    
    PFUser *user = [PFUser currentUser];
    
    image = [self fixOrientation:image];
    
    NSData *imageData = UIImagePNGRepresentation(image);
    
    float length = (int)[imageData length];
    float ratio = length/4000000.0;
    
    if (ratio > 1.0)
    {
        UIImage* scaledImage = [self scaleImage:image ratio:ratio];
        imageData = [NSData dataWithData:UIImagePNGRepresentation(scaledImage)];
    }
    
    PFFile *imageFile = [PFFile fileWithName:@"image.png" data:imageData];

    PFObject *postObject = [PFObject objectWithClassName:@"Posts"];
    postObject[@"EventId"] = object.objectId;
    postObject[@"UserId"] = user.objectId;
    NSRange needleRange = NSMakeRange(0, 1);
    @try {
        postObject[@"UserName"] = [NSString stringWithFormat:@"%@ %@", user[@"FirstName"], [user[@"LastName"] substringWithRange:needleRange]];
    }
    @catch (NSException *exception) {
        postObject[@"UserName"] = [NSString stringWithFormat:@"%@ %@", user[@"FirstName"], user[@"LastName"]];
    }
    postObject[@"Comment"] = postText.text;
    postObject[@"Image"] = imageFile;
    if(image != nil)
        postObject[@"hasImage"] = [NSNumber numberWithBool:TRUE];
    else
        postObject[@"hasImage"] = [NSNumber numberWithBool:FALSE];
    
    //[self showWaiting];
    
    [parse makePost:postObject];
    [self moveOutAddPost];
}

-(void)photolibrarypicker
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.4];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    libraryorcamera.alpha = 1.0;
    [UIView commitAnimations];
    [postText resignFirstResponder];
}

-(void)photolibrarypickerclose
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.4];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    libraryorcamera.alpha = 0.0;
    [UIView commitAnimations];
}

-(void)photo
{
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.allowsEditing = NO;
    
    [self presentViewController:imagePicker
                       animated:YES completion:nil];
}

-(void)library
{
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.allowsEditing = NO;
    
    [self presentViewController:imagePicker
                       animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [photoButton setImage:chosenImage forState:UIControlStateNormal];
    [[photoButton imageView] setContentMode:UIViewContentModeScaleAspectFill];
    image = photoButton.imageView.image;
    [self photolibrarypickerclose];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [photoButton setImage:nil forState:UIControlStateNormal];
    image = nil;
    [self photolibrarypickerclose];
}

-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollOffset = scrollView.contentOffset.y;
    float contentOffset = posts.contentOffset.y;
    float backgroundOffset = background.contentOffset.y;
    float newOffset = backgroundOffset+(1.5*contentOffset);
    
    if(scrollView.contentOffset.y > scrollView.contentOffset.y-180 && !refreshing && items != 0)
    {
        refreshing = YES;
        [self pagingEvents];
    }
    
    if (scrollView.tag == 2 && contentOffset > 0 && backgroundOffset < 238)
    {
        if(newOffset > 238)
        {
            newOffset = 238;
        }
        
        if(newOffset < 25)
        {
            title.alpha = .96 - (newOffset/24.0);
            name.alpha = .96 - (newOffset/24.0);
            description.alpha = .96 - (newOffset/24.0);
            locationPointer.alpha = .96 - (newOffset/24.0);
            location.alpha = .96 - (newOffset/24.0);
            address.alpha = .96 - (newOffset/24.0);
            time.alpha = .96 - (newOffset/24.0);
            url.alpha = .96 - (newOffset/24.0);
            headerTitle.alpha = 1;
            headerView.alpha = 1;
        }
        
        if(newOffset < 80)
        {
            float center = 43.0 - (28.0 * (newOffset/80.0));
            float scale = 1.0 - (.20 * (newOffset/80.0));
            avatar.transform = CGAffineTransformMakeScale(scale, scale);
            map.center = CGPointMake(width/2, center);
        }
        
        if(newOffset > 70)
        {
            [self.view bringSubviewToFront:map];
            [self.view bringSubviewToFront:back];
            [self.view bringSubviewToFront:post];
            [self.view bringSubviewToFront:editButton];

            float center = 112-(newOffset-70);
            avatar.center = CGPointMake(54, center);
        }
        background.contentOffset = CGPointMake(0,newOffset);
        posts.contentOffset = CGPointMake(0, 0);
    }
    else if(scrollView.tag == 2 && scrollOffset < 1)
    {
        if (backgroundOffset > 1)
        {
            float newOffset = backgroundOffset+(1.5*contentOffset);
            if(backgroundOffset < 1)
            {
                newOffset = 0;
            }
            background.contentOffset = CGPointMake(0, newOffset);
            posts.contentOffset = CGPointMake(0, 0);
        }
        else
        {
        if (newOffset < -75 && !refreshing)
        {
            refreshing = YES;
            [parse getPostList:object];
        }
        else if (newOffset < -5)
        {
            pullRefresh.alpha = newOffset/-50.0;
        }
        else
        {
            pullRefresh.alpha = 0.0;
        }
        }

    }
    else if(newOffset < 238 && contentOffset < 0)
    {
        if(newOffset < 70)
        {
            [self.view bringSubviewToFront:avatar];
            float center = 15.0 + (28.0 * (1.0-(newOffset/70.0)));
            if(center > 44)
            {
                center = 44;
            }
            float scale = .80 + (.20 * (1.0-(newOffset/70.0)));
            if(scale > 1)
            {
                scale = 1.0;
            }
            avatar.transform = CGAffineTransformMakeScale(scale, scale);
            map.center = CGPointMake(width/2, center);
            avatar.center = CGPointMake(54, 112);
        }
        else if(newOffset < 160)
        {
            float center = 20.0 + (92.0*((160.0-newOffset)/90.0));
            if (center > 112)
            {
                center = 112;
            }
            avatar.center = CGPointMake(54, center);
        }
        
        if(newOffset < 25)
        {
            title.alpha = .99 - (newOffset/24.0);
            name.alpha = .99 - (newOffset/24.0);
            description.alpha = .99 - (newOffset/24.0);
            locationPointer.alpha = .99 - (newOffset/24.0);
            location.alpha = .99 - (newOffset/24.0);
            address.alpha = .99 - (newOffset/24.0);
            time.alpha = .99 - (newOffset/24.0);
            url.alpha = .99 - (newOffset/24.0);
            headerTitle.alpha = (newOffset/24.0);
            headerView.alpha = (newOffset/24.0);
        }
    }
}

-(void)scrollReset
{
    posts.contentOffset = CGPointMake(0,0);
    background.contentOffset = CGPointMake(0,0);
    title.alpha = 1.0;
    name.alpha = 1.0;
    description.alpha = 1.0;
    locationPointer.alpha = 1.0;
    location.alpha = 1.0;
    address.alpha = 1.0;
    time.alpha = 1.0;
    url.alpha = 1.0;
    headerTitle.alpha = 0.0;
    headerView.alpha = 0.0;
    map.center = CGPointMake(width/2, 45);
    avatar.center = CGPointMake(54, 112);
    avatar.transform = CGAffineTransformMakeScale(1, 1);
    [self.view bringSubviewToFront:avatar];
}

-(void)pagingEvents
{
    if (items >= 10 && items%10 == 0)
    {
        int skip = items/10.0;
        [parse pagingPosts:object skip:skip];
    }
}

-(void)goToEventComments:(PFObject *)event
{
    [parse getCommentList:event];
}

-(void)moveOut
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    self.view.center = CGPointMake(-(width/2.0), height/2.0);
    [UIView commitAnimations];
    [self scrollReset];
}

-(void)moveIn
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    self.view.center = CGPointMake(width/2.0, (height/2.0));
    [UIView commitAnimations];
}

-(void)moveOutAddPost
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    addPost.center = CGPointMake(width/2.0, (height*2.0));
    [UIView commitAnimations];
    [postText resignFirstResponder];
    [postText setText:@""];
    image = nil;
    [photoButton setImage:nil forState:UIControlStateNormal];
}

-(void)moveInAddPost
{
    [self.view bringSubviewToFront:addPost];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    addPost.center = CGPointMake(width/2.0, height/2.0);
    [UIView commitAnimations];
    [postText becomeFirstResponder];
    [self.view bringSubviewToFront:libraryorcamera];
}

-(void)resetPosts
{
    shift = 0;
    items = 0;
    [[posts subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
}

#define MAX_LENGTH 140 // Whatever your limit is
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSUInteger newLength = (textView.text.length - range.length) + text.length;
    if(newLength <= MAX_LENGTH)
    {
        return YES;
    } else {
        NSUInteger emptySpace = MAX_LENGTH - (textView.text.length - range.length);
        textView.text = [[[textView.text substringToIndex:range.location]
                          stringByAppendingString:[text substringToIndex:emptySpace]]
                         stringByAppendingString:[textView.text substringFromIndex:(range.location + range.length)]];
        return NO;
    }
}

-(void)setUserPoint:(PFGeoPoint*)center
{
    userPoint = center;
}

-(void)imageTap:(UIImage*)img
{
    [self.view bringSubviewToFront:imageBackground];
    [self.view bringSubviewToFront:popoutImage];
    [self.view bringSubviewToFront:closePopout];
    [popoutImage setImage:img];
    popoutImage.contentMode = UIViewContentModeScaleAspectFill;
    popoutImage.layer.masksToBounds = YES;
    popoutImage.hidden = NO;
    imageBackground.hidden = NO;
    closePopout.hidden = NO;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    popoutImage.alpha = 1.0;
    imageBackground.alpha = .975;
    closePopout.alpha = 1.0;
    [UIView commitAnimations];
}

-(void)popoutDismiss
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDelegate:self];
    popoutImage.alpha = 0;
    imageBackground.alpha = 0;
    closePopout.alpha = 0;
    [UIView commitAnimations];
    popoutImage.hidden = YES;
    imageBackground.hidden = YES;
    closePopout.hidden = YES;
}

-(UIImage*)scaleImage:(UIImage*)selectedImage ratio:(float)ratio
{
    CGImageRef simage = selectedImage.CGImage;
    
    float pwidth = CGImageGetWidth(simage)/ratio;
    float pheight = CGImageGetHeight(simage)/ratio;
    float bitsPerComponent = CGImageGetBitsPerComponent(simage);
    float bytesPerRow = CGImageGetBytesPerRow(simage);
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(simage);
    float bitmapInfo = CGImageGetBitmapInfo(simage);
    
    CGContextRef context = CGBitmapContextCreate(nil, pwidth, pheight, bitsPerComponent, bytesPerRow, colorSpace, bitmapInfo);
    
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    
    CGContextDrawImage(context, CGRectMake(0, 0, pwidth, pheight), simage);
    
    UIImage* scaledImage = [[UIImage alloc]initWithCGImage:CGBitmapContextCreateImage(context)];
    
    return scaledImage;
}

- (UIImage *)fixOrientation:(UIImage*)newImage {
    
    // No-op if the orientation is already correct
    if (newImage.imageOrientation == UIImageOrientationUp) return newImage;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (newImage.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, newImage.size.width, newImage.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, newImage.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, newImage.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (newImage.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, newImage.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, newImage.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, newImage.size.width, newImage.size.height,
                                             CGImageGetBitsPerComponent(newImage.CGImage), 0,
                                             CGImageGetColorSpace(newImage.CGImage),
                                             CGImageGetBitmapInfo(newImage.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (newImage.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,newImage.size.height,newImage.size.width), newImage.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,newImage.size.width,newImage.size.height), newImage.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

@end
