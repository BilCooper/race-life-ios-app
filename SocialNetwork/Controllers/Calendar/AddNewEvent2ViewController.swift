//
//  AddNewEvent2ViewController.swift
//  SocialNetwork
//
//  Created by Yasir on 1/11/18.
//  Copyright © 2018 Sugarsage. All rights reserved.
//

import UIKit
import MapKit


@objc protocol AddNewEvent2ViewControllerDelegate {
    
    func AddNewEvent2ViewControllerDidPressCloseButton()
    
}


class AddNewEvent2ViewController: UIViewController,UITextViewDelegate,MKMapViewDelegate,UIGestureRecognizerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate {

    var delegate =  AddNewEvent2ViewControllerDelegate.self as? AddNewEvent2ViewControllerDelegate
    
    var userLocation:CLLocation = CLLocation()
    
    let durationAnim = 1.0
    @IBOutlet weak var btnClose: UIButton!

    let dicObject:NSMutableDictionary = NSMutableDictionary()
    
    let dummyTVText = "University 5K Fundraiser..."
    let dummyTVTextMore = "www.fundmyadventure.com"
    //www.fundmyadventure.com
    //MARK: Event Name
    @IBOutlet weak var eventNameView: UIView!
    @IBOutlet weak var tvEventName: UITextView!
    @IBOutlet weak var lblEventName: UILabel!
    @IBOutlet weak var constEventNameTrailing: NSLayoutConstraint!
    @IBOutlet weak var constEventNameLeading: NSLayoutConstraint!
    
    @IBAction func btnCancelEventNamePressed(_ sender: UIButton) {
        
        self.overAndOut()
    }
    
    @IBAction func btnNextEventNamePressed(_ sender: UIButton) {
        
        if self.tvEventName.text.characters.count == 0 || self.tvEventName.text == dummyTVText {
            return
        }
        
        self.dicObject.setObject(self.tvEventName.text, forKey:"titleIn" as NSCopying)
        
        self.eventAboutView.isHidden = false
        self.view.layoutIfNeeded()
        self.constEventNameTrailing.constant =  500
        self.constEventNameLeading.constant = -500
        
        self.constEventAboutTrailing.constant =  8
        self.constEventAboutLeading.constant = 8
        
        
        UIView.animate(withDuration: durationAnim) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    //MARK: Event About
    @IBOutlet weak var eventAboutView: UIView!
    @IBOutlet weak var lblEventAbout: UILabel!
    @IBOutlet weak var tvEventAbout: UITextView!
    @IBOutlet weak var constEventAboutTrailing: NSLayoutConstraint!
    @IBOutlet weak var constEventAboutLeading: NSLayoutConstraint!
    
    @IBAction func btnBackEventAboutPressed(_ sender: UIButton) {
        
        //self.eventAboutView.isHidden = true
        self.eventNameView.isHidden = false
        
        self.view.layoutIfNeeded()
        self.constEventNameTrailing.constant =  10
        self.constEventNameLeading.constant = 10
        UIView.animate(withDuration:durationAnim) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnNextEventAboutPressed(_ sender: UIButton) {
        
        if self.tvEventAbout.text.characters.count == 0 || self.tvEventAbout.text == dummyTVText {
            return
        }
        
        self.dicObject.setObject(self.tvEventAbout.text, forKey:"descriptionIn" as NSCopying)
        
        self.eventDateView.isHidden = false
        
        self.view.layoutIfNeeded()
        self.constEventAboutTrailing.constant =  500
        self.constEventAboutLeading.constant = -500
        
        self.constEventDateTrailing.constant =  8
        self.constEventDateLeading.constant = 8
        
        UIView.animate(withDuration:durationAnim) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    //MARK: Event Date
    @IBOutlet weak var eventDateView: UIView!
    @IBOutlet weak var lblDateEvent: UILabel!
    @IBOutlet weak var dpEventDate: UIDatePicker!
    
    
    @IBOutlet weak var constEventDateTrailing: NSLayoutConstraint!
    @IBOutlet weak var constEventDateLeading: NSLayoutConstraint!
    
    
    @IBAction func btnBackEventDatePressed(_ sender: UIButton) {
        
        self.eventAboutView.isHidden = false
        
        self.view.layoutIfNeeded()
        self.constEventAboutTrailing.constant =  10
        self.constEventAboutLeading.constant = 10
        UIView.animate(withDuration:durationAnim) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func btnNextEventDatePressed(_ sender: UIButton) {
        
        self.eventWhereView.isHidden = false
        self.view.layoutIfNeeded()
        self.constEventDateTrailing.constant =  500
        self.constEventDateLeading.constant = -500
        
        self.constEventWhereTrailing.constant =  8
        self.constEventWhereLeading.constant = 8
        
        UIView.animate(withDuration:durationAnim) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        
        let currentDate = self.dpEventDate.date;
        let currentDateStr = self.getStringFromDate(dateIn: currentDate)
        
        self.dicObject.setObject(currentDateStr, forKey: "dateIn" as NSCopying)

        let timeAndDate = self.getTimeFromDate(dateIn: currentDate)
        
        let arr = timeAndDate.components(separatedBy:" ")
        
        let onlyTimeString = "\(arr[1]) \(arr[2])"
        
        self.dicObject.setObject(onlyTimeString, forKey: "fromIn" as NSCopying)
    }
    
    
    
    func getStringFromDate(dateIn:Date)->String{
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from:dateIn)
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        
        //then again set the date format whhich type of output you need
        //formatter.dateFormat = "dd-MMM-yyyy"
        formatter.dateFormat = "MMMM dd, yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        print("Selected-Date == \(myStringafd)")
        
        return myStringafd
    }
    
    func getTimeFromDate(dateIn:Date)->String{
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd h:mm a"
        
        let myString = formatter.string(from:dateIn)
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        
        //then again set the date format whhich type of output you need
        //formatter.dateFormat = "dd-MMM-yyyy"
        //formatter.dateFormat = "MMMM dd, yyyy"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        print("Selected-Date-Time == \(myStringafd)")
        
        return myStringafd
    }
    
    
    //MARK: Event Where
    
    var items = [MKMapItem]()
    var selectedIndex:Int = -1
    
    @IBOutlet var tableViewLocations: UITableView!
    
    
    let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
    
    @IBOutlet weak var eventWhereView: UIView!
    @IBOutlet weak var lblEventWhere: UILabel!
    @IBOutlet weak var searchBarEventWhere: UISearchBar!
    @IBOutlet var mapKit: MKMapView!
    
    var selectedLatitude:CGFloat = 21.282778
    var selectedLongitude:CGFloat = -157.829444
    
    
    @IBOutlet weak var constEventWhereTrailing: NSLayoutConstraint!
    @IBOutlet weak var constEventWhereLeading: NSLayoutConstraint!
    
    @IBAction func btnBackEventWherePressed(_ sender: UIButton) {
        self.view.layoutIfNeeded()
        self.constEventDateTrailing.constant =  10
        self.constEventDateLeading.constant = 10
        UIView.animate(withDuration:durationAnim) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnNextEventWherePressed(_ sender: UIButton) {
        
        if self.selectedLongitude == 21.282778 || self.selectedLatitude == -157.829444{

            print("no lat long selected")
            return
        }
        
        
        guard let locationGiven = self.dicObject.object(forKey: "locationNameIn")
            else
        {
            print("location not provided yet")
            return
        }
        
        
        self.dicObject.setObject(locationGiven, forKey: "locationNameIn" as NSCopying)
        self.dicObject.setObject(self.selectedLatitude, forKey:"latitudeIn" as NSCopying)
        self.dicObject.setObject(self.selectedLongitude, forKey:"longitudeIn" as NSCopying)
        
        self.eventMoreView.isHidden = false
        self.view.layoutIfNeeded()
        self.constEventWhereTrailing.constant =  500
        self.constEventWhereLeading.constant = -500
        
        self.constEventMoreTrailing.constant =  8
        self.constEventMoreLeading.constant = 8
        
        UIView.animate(withDuration:durationAnim) {
            self.view.layoutIfNeeded()
        }
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapKit.setRegion(coordinateRegion, animated: true)
    }
    
    
    func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizerState.ended {
            let touchLocation = gestureReconizer.location(in: self.mapKit)
            let locationCoordinate = mapKit.convert(touchLocation,toCoordinateFrom: self.mapKit)
            print("Tapped at lat: \(locationCoordinate.latitude) long: \(locationCoordinate.longitude)")
            
            self.selectedLatitude = CGFloat(locationCoordinate.latitude)
            self.selectedLongitude = CGFloat(locationCoordinate.longitude)
            
            return
        }
        if gestureReconizer.state != UIGestureRecognizerState.began {
            return
        }
    }
    
    //MARK:- TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let someCell = tableView.dequeueReusableCell(withIdentifier:"cell")
        
        let mapItem = self.items[indexPath.row]
        
        someCell?.textLabel?.text = mapItem .placemark.name
        someCell?.detailTextLabel?.text = mapItem.placemark.title
        if (selectedIndex == indexPath.row){
            someCell?.accessoryType = UITableViewCellAccessoryType.checkmark
        }else{
            someCell?.accessoryType = UITableViewCellAccessoryType.none
        }

        return someCell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.tableViewLocations.reloadData()
        
        let mapItem = self.items[selectedIndex]
        
        guard let nameOfPlace = mapItem.placemark.name
        else{
            print("placemark name not found")
            return
        }
        
        self.dicObject.setObject(nameOfPlace, forKey: "locationNameIn" as NSCopying)
        
        let lat1 = mapItem.placemark.coordinate.latitude
        let long1 = mapItem.placemark.coordinate.longitude
        
        self.selectedLatitude = CGFloat(lat1);
        self.selectedLongitude = CGFloat(long1);
        
        self.dicObject.setObject(lat1, forKey:"latitudeIn" as NSCopying)
        self.dicObject.setObject(long1, forKey:"longitudeIn" as NSCopying)
        
        let span1 = MKCoordinateSpan(latitudeDelta:0.09, longitudeDelta:0.09)
        
        let loc1 = CLLocationCoordinate2D(latitude:lat1, longitude:long1)
        
        let region1 = MKCoordinateRegion(center: loc1, span: span1)
        
        self.mapKit.setRegion(region1, animated: true)
        
        self.tableViewLocations.isHidden = true
        
        
        let anno1 = MKPointAnnotation()
        anno1.coordinate = loc1
        
        
        self.mapKit.addAnnotation(anno1)
        
        self.searchBarEventWhere.resignFirstResponder()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation{
            return nil;
        }else{
            let pinIdent = "Pin";
            var pinView: MKPinAnnotationView;
            if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: pinIdent) as? MKPinAnnotationView {
                dequeuedView.annotation = annotation;
                pinView = dequeuedView;
            }else{
                pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: pinIdent);
                
            }
            return pinView;
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        
        self.tableViewLocations.isHidden = true
        self.items.removeAll()
        self.tableViewLocations.reloadData()
        
        guard let text1 = searchBar.text else {
            return
        }
        self.searchLocation(text:text1)
    }
    
    func searchLocation(text:String){
        let searchRequest = MKLocalSearchRequest()
        searchRequest.naturalLanguageQuery = text
        
        let localSearch = MKLocalSearch(request: searchRequest)
        localSearch.start { (response:MKLocalSearchResponse?, error:Error?) in
            
            if(error != nil){
                print("some error in search of local MKLocalSearch")
                self.items.removeAll()
                self.tableViewLocations.isHidden = true
            }else{
                self.tableViewLocations.isHidden = false
                self.items = (response?.mapItems)!
            }
            
            self.tableViewLocations.reloadData()
        }
    }
    
    //MARK: Event More
    @IBOutlet weak var eventMoreView: UIView!
    @IBOutlet var lblEventMore: UILabel!
    @IBOutlet var tvEventMore: UITextView!
    @IBOutlet weak var constEventMoreTrailing: NSLayoutConstraint!
    @IBOutlet weak var constEventMoreLeading: NSLayoutConstraint!
    
    func didTapOnMoreTextView(){
        print("didTapOnMoreTextView")
    
        self.tvEventMore.isEditable = true
        self.tvEventMore.becomeFirstResponder()
    }
    
    @IBAction func btnBackEventMorePressed(_ sender: UIButton) {
        
        self.view.layoutIfNeeded()
        self.constEventWhereTrailing.constant =  10
        self.constEventWhereLeading.constant = 10
        
        UIView.animate(withDuration:durationAnim) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnNextEventMorePressed(_ sender: UIButton) {
        
        if self.tvEventMore.text.characters.count == 0 || self.tvEventMore.text == dummyTVTextMore {
            return
        }
        
        self.dicObject.setObject(self.tvEventMore.text, forKey:"moreIn" as NSCopying)
        
        self.eventLogoView.isHidden = false
        self.view.layoutIfNeeded()
        self.constEventMoreTrailing.constant =  500
        self.constEventMoreLeading.constant = -500
        
        self.constEventLogoTrailing.constant =  8
        self.constEventLogoLeading.constant = 8
        
        UIView.animate(withDuration:durationAnim) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: Event Logo
    @IBOutlet weak var eventLogoView: UIView!
    @IBOutlet var lblEventLogo: UILabel!
    @IBOutlet var imgEventLogo: UIImageView!
    
    @IBOutlet weak var constEventLogoTrailing: NSLayoutConstraint!
    @IBOutlet weak var constEventLogoLeading: NSLayoutConstraint!
    
    
    @IBAction func btnBackEventLogoPressed(_ sender: UIButton) {
        self.view.layoutIfNeeded()
        self.constEventMoreTrailing.constant =  10
        self.constEventMoreLeading.constant = 10
        UIView.animate(withDuration:durationAnim) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func btnSelectImagePressed(_ sender: UIButton) {
        
        
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            
            let cont = UIAlertController(title: "", message:"Please select photo from", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let camera = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (action) in
                
                let imgPicker = UIImagePickerController()
                imgPicker.sourceType = UIImagePickerControllerSourceType.camera
                imgPicker.allowsEditing = true
                imgPicker.delegate = self
                self.present(imgPicker, animated: true, completion: nil)
                
            }
            cont.addAction(camera)
            
            let gallery = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) { (action) in
                let imgPicker = UIImagePickerController()
                imgPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                imgPicker.allowsEditing = true
                imgPicker.delegate = self
                self.present(imgPicker, animated: true, completion: nil)
            }
            
            cont.addAction(gallery)
            
            let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (action) in
              print("Cancelled image picker")
            })
            cont.addAction(cancel)
            
            self.present(cont, animated: true, completion: nil)
        }else{
            
            let imgPicker = UIImagePickerController()
            imgPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imgPicker.allowsEditing = true
            imgPicker.delegate = self
            self.present(imgPicker, animated: true, completion: nil)
            
        }
        
        
        
        /*
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            
            let imgPicker = UIImagePickerController()
            imgPicker.sourceType = UIImagePickerControllerSourceType.camera
            imgPicker.allowsEditing = true
            imgPicker.delegate = self
            self.present(imgPicker, animated: true, completion: nil)
            
        }else{
            
            let imgPicker = UIImagePickerController()
            imgPicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imgPicker.allowsEditing = true
            imgPicker.delegate = self
            self.present(imgPicker, animated: true, completion: nil)
        }
        */
        
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        self.imgEventLogo.contentMode = .scaleAspectFit
        self.imgEventLogo.image = chosenImage
        dismiss(animated:true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    @IBAction func btnDoneEventLogoPressed(_ sender: UIButton) {
        
        let helper:HelperParse = HelperParse()
        
        if helper.image(UIImage.init(named:"placeholderImg"), isEqualTo: self.imgEventLogo.image){
            
            print("Image not selected")
            
            return
        }
        
        let imgData = UIImagePNGRepresentation(self.imgEventLogo.image!)
        self.dicObject.setObject(imgData, forKey: "imageDataIn" as NSCopying)
        
        let loading = MBProgressHUD.showAdded(to: self.view, animated: true)
        loading?.labelText = "Saving...";
        
        
        helper.saveObject(withPara: self.dicObject as! [String:Any]) { (status) in
            print("status == \(String(describing: status!))")
            MBProgressHUD.hide(for: self.view, animated: true)
            
            self.btnClosePressed(self.btnClose)
        }
        
        //save everything
    }
    
    
    func populateDummyValues(){
        self.tvEventName.text = "macbook"
        self.tvEventAbout.text = "macbook about"
        
        
        self.selectedLatitude = 1.23456
        self.selectedLongitude = 2.23456
        self.dicObject.setObject("Gilget", forKey: "locationNameIn" as NSCopying)
        
        self.tvEventMore.text = "www.mybook.com"
        
        self.imgEventLogo.image = UIImage.init(named:"editBtn")
        
        
        //self.dicObject.setObject(locationGiven, forKey: "locationNameIn" as NSCopying)
        //self.dicObject.setObject(self.selectedLatitude, forKey:"latitudeIn" as NSCopying)
        //self.dicObject.setObject(self.selectedLongitude, forKey:"longitudeIn" as NSCopying)
    }
    
    //MARK: view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        self.eventNameView.isHidden = false
        self.tvEventName.delegate = self
        self.tvEventName.text = dummyTVText

        self.eventAboutView.isHidden = true
        self.tvEventAbout.delegate = self;
        self.tvEventAbout.text = dummyTVText
        
        self.eventDateView.isHidden = true
        self.eventWhereView.isHidden = true
        
        self.eventMoreView.isHidden = true
        self.tvEventMore.delegate = self
        self.tvEventMore.text = dummyTVTextMore
        
        self.eventLogoView.isHidden = true
        
        self.constEventAboutTrailing.constant =  -500
        self.constEventAboutLeading.constant = 500
        
        
        self.constEventDateTrailing.constant =  -500
        self.constEventDateLeading.constant = +500
        
        self.constEventWhereTrailing.constant =  -500
        self.constEventWhereLeading.constant = +500
        
        self.constEventMoreTrailing.constant =  -500
        self.constEventMoreLeading.constant = +500
        
        self.constEventLogoTrailing.constant =  -500
        self.constEventLogoLeading.constant = +500
        
        let tap1 = UITapGestureRecognizer(target: self, action:"didTapOnMoreTextView")
        tap1.numberOfTapsRequired = 1
        tap1.delegate = self
        self.tvEventMore.isEditable = false
        self.tvEventMore.addGestureRecognizer(tap1)
        
        //self.populateDummyValues()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        self.setupForCurrentUserLocation()
    }
    
    func setupForCurrentUserLocation(){
        let span1 = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region1 = MKCoordinateRegion(center:self.userLocation.coordinate, span:span1)
        self.mapKit.setRegion(region1, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnClosePressed(_ sender: UIButton) {
        
        
        guard let del1 = self.delegate else {
            
            print("close delegate not set")
            return
        }
        
        del1.AddNewEvent2ViewControllerDidPressCloseButton()
        
        
        self.overAndOut()
        
        
    }
    
    
    func overAndOut(){
        if self.navigationController?.topViewController == self {
            
            self.navigationController?.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    /*
    
    PFObject *event = [PFObject objectWithClassName:@"Event"];
    [event setObject:[PFUser currentUser] forKey:@"user"];
    [event setObject:self.txtTitle.text forKey:@"title"];
    [event setObject:self.txtDescription.text?:@"" forKey:@"description"];
    [event setObject:self.txtDate.text forKey:@"date"];
    [event setObject:self.txtFrom.text forKey:@"from"];
    [event setObject:@"11:59 PM" forKey:@"to"];
    [event setObject:self.txtLocation.text forKey:@"locationname"];
    PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:self.latitude longitude:self.longitude];
    [event setObject:geoPoint forKey:@"location"];
    [event setObject:self.txtUrl.text?:@"" forKey:@"website"];
    if(self.imageData!=nil)
    {
    PFFile *picture = [PFFile fileWithData:self.imageData];
    [event setObject:picture forKey:@"picture"];
    }
    [event saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
    
    if (succeeded) {
    NSLog(@"Event saved successfully");
    }
    
    }];
    
    */
    
    
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //print("textViewDidBeginEditing")
        if textView == self.tvEventName  && textView.text ==  dummyTVText{
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
        if textView == self.tvEventAbout  && textView.text ==  dummyTVText{
            textView.text = ""
            textView.textColor = UIColor.black
        }
        
        if textView == self.tvEventMore  && textView.text ==  dummyTVTextMore{
            textView.text = ""
            //textView.textColor = UIColor.black
        }
        
        if(textView == self.tvEventMore){
            
            self.tvEventMore.dataDetectorTypes = UIDataDetectorTypes.link
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == self.tvEventName && textView.text.characters.count == 0 {
            self.tvEventName.text = dummyTVText
            textView.textColor = UIColor.lightGray
        }
        
        if textView == self.tvEventAbout && textView.text.characters.count == 0 {
            self.tvEventAbout.text = dummyTVText
            textView.textColor = UIColor.lightGray
        }
        
        if textView == self.tvEventMore && textView.text.characters.count == 0 {
            self.tvEventMore.text = dummyTVTextMore
            textView.textColor = UIColor.lightGray
        }
        
        
        if (textView == self.tvEventMore) {
            self.tvEventMore.isEditable = false
            self.tvEventMore.dataDetectorTypes = UIDataDetectorTypes.link
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }


}
