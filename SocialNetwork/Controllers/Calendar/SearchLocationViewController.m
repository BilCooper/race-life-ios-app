//
//  SearchLocationViewController.m
//  Do-Invision
//
//  Created by Rupesh on 4/21/17.
//  Copyright © 2017 Mark Feaver. All rights reserved.
//

#import "SearchLocationViewController.h"
#import "AddNewEventViewController.h"
@import MapKit;

@interface SearchLocationViewController ()
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,weak)IBOutlet UISearchBar *searchBar;
@property(nonatomic,strong)NSArray *items;
@property(nonatomic,assign)int selectedIndex;

@end

@implementation SearchLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _selectedIndex = -1;
    // Do any additional setup after loading the view.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _items.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    
  //  NSLog(@"Name: %@, Placemark title: %@", [mapItem name], [[mapItem placemark] title]);
    MKMapItem *item = [_items objectAtIndex:indexPath.row];
    cell.textLabel.text=[NSString stringWithFormat:@"%@",[[item placemark] name]];
    cell.detailTextLabel.text = [[item placemark] title];
    if(_selectedIndex==indexPath.row)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;

    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedIndex = (int)indexPath.row;
    [_tableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self searchLocation:_searchBar.text];

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self searchLocation:_searchBar.text];
    [searchBar resignFirstResponder];
}
-(void)searchLocation:(NSString*)text
{
    // Create a search request with a string
    MKLocalSearchRequest *searchRequest = [[MKLocalSearchRequest alloc] init];
    [searchRequest setNaturalLanguageQuery:text];
   // CLLocationCoordinate2D parisCenter = CLLocationCoordinate2DMake(48.8566667, 2.3509871);
   // MKCoordinateRegion parisRegion = MKCoordinateRegionMakeWithDistance(parisCenter, 15000, 15000);
  //  [searchRequest setRegion:parisRegion];    // Create the local search to perform the search
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:searchRequest];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        if (!error) {
            
            self.items = response.mapItems;
            //for (MKMapItem *mapItem in [response mapItems]) {
             //   NSLog(@"Name: %@, Placemark title: %@", [mapItem name], [[mapItem placemark] title]);
           // }
        } else {
            NSLog(@"Search Request Error: %@", [error localizedDescription]);
        }
        [self.tableView reloadData];
    }];

}
-(IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:NULL];

}
-(IBAction)done:(id)sender
{
    MKMapItem *item = [_items objectAtIndex:_selectedIndex];
    [_delegate locationSelected:[[item placemark] name] withLatitude:[[item placemark] location].coordinate.latitude andLongitude:[[item placemark] location].coordinate.longitude];
    [self dismissViewControllerAnimated:YES completion:NULL];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
