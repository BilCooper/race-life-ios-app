//
//  CustomCalendarEvenTableViewCell.m
//  SocialNetwork
//
//  Created by Yasir on 12/22/17.
//  Copyright © 2017 Sugarsage. All rights reserved.
//

#import "CustomCalendarEvenTableViewCell.h"

@implementation CustomCalendarEvenTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.imgCell.layer.cornerRadius = self.imgCell.frame.size.width/2;
    self.imgCell.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
